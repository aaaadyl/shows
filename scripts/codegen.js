const { generateApi } = require('swagger-typescript-api');
const path = require('path');

const fileName = 'api.ts';
const outputDir = path.resolve(process.cwd(), './src/shared/api/common');
const urlToSwaggerSchema = `https://api-prod-shows.kg/api/v1/swagger/?format=openapi`;

const pathToTemplate = path.resolve(process.cwd(), 'node_modules', 'effector-http-api/codegen-template');

generateApi({
  name: fileName,
  output: outputDir,
  url: urlToSwaggerSchema,
  httpClientType: 'axios',
  generateClient: true,
  templates: pathToTemplate,
});
