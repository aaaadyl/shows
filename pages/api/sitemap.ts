import { SitemapStream, streamToPromise } from 'sitemap';
import { NextApiRequest, NextApiResponse } from 'next';
import { Readable } from 'stream';

const links = [{ url: process.env.BASE_URL, changefreq: 'daily', priority: 1.0, lastmod: new Date() }];

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const stream = new SitemapStream({ hostname: `https://${req.headers.host}` });
    res.writeHead(200, { 'Content-Type': 'application/xml' });
    const xmlStream = await streamToPromise(Readable.from(links).pipe(stream)).then((data) => data.toString());
    res.end(xmlStream);
  } catch (e) {
    res.status(500).end();
  }
};
