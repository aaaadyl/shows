import { NextApiRequest, NextApiResponse } from 'next';

export default (req: NextApiRequest, res: NextApiResponse) => {
  res.send(`#All
User-agent: *
Disallow: /
Disallow: /admin/
Disallow: /_next/
Disallow: /services/null

Allow: */static
Allow: /*/*.js
Allow: /*/*.css
Allow: /*/*.png
Allow: /*/*.jpg
Allow: /*/*.jpeg
Allow: /*/*.gif

Sitemap: ${process.env.BASE_URL}/sitemap.xml
    `);
};
