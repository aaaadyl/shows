import { DoramaPage, doramaModel } from 'pages/doramas';
import { createGIP } from 'pages/shared';

DoramaPage.getInitialProps = createGIP({
  pageEvent: doramaModel.pageStarted,
});

export default DoramaPage;
