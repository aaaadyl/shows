import { FilmsPage, filmsModel } from 'pages/films';
import { createGSSP } from 'pages/shared';

export const getServerSideProps = createGSSP({
  pageEvent: filmsModel.pageStarted,
});

export default FilmsPage;
