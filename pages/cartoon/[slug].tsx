/* eslint-disable boundaries/element-types */
import { commonMovieDetailModel, MovieDetail } from 'entities/movie';
import { createGSSP } from 'pages/shared';

export const getServerSideProps = createGSSP({
  pageEvent: commonMovieDetailModel.events.fetchMainMovie,
});

export { MovieDetail as default };
