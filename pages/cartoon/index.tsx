import { CartoonsPage, cartoonsModel } from 'pages/cartoons';
import { createGSSP } from 'pages/shared';

export const getServerSideProps = createGSSP({
  pageEvent: cartoonsModel.pageStarted,
});

export default CartoonsPage;
