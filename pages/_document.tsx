import { Html, Head, Main, NextScript } from 'next/document';

const Document = () => {
  return (
    <Html className="page" lang="ru">
      <Head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />

        <link rel="apple-touch-icon" sizes="180x180" href="/favicon-package/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-package/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-package/favicon-16x16.png" />
        <link rel="manifest" href="/favicon-package/site.webmanifest" />
        <link rel="mask-icon" href="/favicon-package/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#000000" />
        <meta name="theme-color" content="#ffffff" />
      </Head>
      {/* clickmap:true, trackLinks:true, accurateTrackBounce:true */}
      <body className="page__body">
        <Main />
        <div id="modal" />
        <NextScript />
      </body>
    </Html>
  );
};

export default Document;
