import { AnimePage, animeModel } from 'pages/anime';
import { createGSSP } from 'pages/shared';

export const getServerSideProps = createGSSP({
  pageEvent: animeModel.pageStarted,
});

export default AnimePage;
