import { createGSSP } from 'pages/shared';
import AnimePage from './anime';
import { animeModel } from 'pages/anime';

export const getServerSideProps = createGSSP({
  pageEvent: animeModel.pageStarted,
});

export default AnimePage;
