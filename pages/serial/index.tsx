import { SeriesPage, seriesModel } from 'pages/series';
import { createGSSP } from 'pages/shared';

export const getServerSideProps = createGSSP({
  pageEvent: seriesModel.pageStarted,
});

export default SeriesPage;
