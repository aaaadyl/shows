/* eslint-disable react-hooks/exhaustive-deps */
import clsx from 'clsx';
import { useEffect } from 'react';
import { Filters, filtersModel } from 'features/filters';
import { useToggler } from 'shared/lib';
import { Title, Icon, Button } from 'shared/ui';
import { useElementOnScreen } from '../lib';
import styles from './styles.module.scss';
import { MovieList } from 'shared/api';
import { MovieTypeIDEnum } from 'shared/movies';

interface CatalogProps {
  title: string;
  movies: MovieList[] | null;
  renderItems: (item: MovieList) => JSX.Element;
  loadMore: () => void;
  hasMore: boolean;
  pending: boolean;
  isSeries?: boolean;
  typeOfMovieId: MovieTypeIDEnum;
}

export const Catalog = ({ title, movies, loadMore, hasMore, pending, renderItems, typeOfMovieId }: CatalogProps) => {
  const [buttonRef, isVisible] = useElementOnScreen<HTMLButtonElement>({ rootMargin: '450px' });
  const { open } = useToggler(filtersModel.toggler);

  useEffect(() => {
    if (isVisible) {
      loadMore();
    }
  }, [isVisible]);

  return (
    <section className={styles.section}>
      <div className={clsx('container', styles.container)}>
        <div className={styles.top}>
          <Title className={styles.title}>{title}</Title>
          <button onClick={open} className={clsx('btn-reset', styles.btn)}>
            <Icon type="common" name="filters" />
          </button>
        </div>
        <Filters typeOfMovieId={typeOfMovieId} />
        <div className={styles.grid}>{!!movies?.length && movies.map((item) => renderItems(item))}</div>
        {hasMore && (
          <Button
            disabled
            size="medium"
            variant="gray"
            ref={buttonRef}
            skeletonLoading={pending}
            className={styles.loadMore}
          >
            Показать больше
          </Button>
        )}
      </div>
    </section>
  );
};
