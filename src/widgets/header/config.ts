import { paths } from 'shared/routing';

export const navItems = [
  { text: 'Главная', href: paths.home },
  { text: 'Фильмы', href: paths.films },
  { text: 'Сериалы', href: paths.series },
  { text: 'Мультфильмы', href: paths.cartoons },
  { text: 'Аниме', href: paths.anime },
  { text: 'Дорамы', href: paths.doramas },
  // { text: 'Кыргызча', href: paths.kyrgyz },
];
