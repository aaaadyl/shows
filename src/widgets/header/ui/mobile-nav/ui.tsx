import clsx from 'clsx';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { navItems } from '../../config';
import styles from './styles.module.scss';
import { Button } from 'shared/ui';

export const MobileNav = () => {
  const { pathname } = useRouter();

  return (
    <nav className={`${styles.nav} container`}>
      <ul className={clsx('list-reset', styles.list)}>
        {navItems.map((item) => {
          const isCurrentPage = pathname === item.href;

          return (
            <Link key={item.text} className={clsx(styles.link)} href={item.href}>
              <Button variant={isCurrentPage ? 'primary' : 'gray'} className={styles.item}>
                {item.text}
              </Button>
            </Link>
          );
        })}
      </ul>
    </nav>
  );
};
