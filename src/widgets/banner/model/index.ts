import { attach, combine, createEvent, restore, sample } from 'effector';
import { PageContext } from 'nextjs-effector';
import { commonApi } from 'shared/api';

export const getBanners = createEvent<string>();

export const $offset = 1;
export const $limit = 5;

const $params = combine({ offset: $offset, limit: $limit });

// const getBannerMoviesFx = attach({ effect: commonApi.v1.v1BannerBannerList });
// export const $bannerMovies = restore(getBannerMoviesFx, null);

// sample({
//   clock: getBanners,
//   source: $params,
//   fn: ({ limit, offset }, options) => {
//     return { limit, offset };
//   },
//   target: getBannerMoviesFx,
// });
