/* eslint-disable jsx-a11y/media-has-caption */
import clsx from 'clsx';
import Image from 'next/image';
import Link from 'next/link';
import { useRef } from 'react';
import { paths } from 'shared/routing';
import { Title, MovieRating } from 'shared/ui';
import styles from './styles.module.scss';
// import { SettingsBannerItem } from 'shared/api/common/api';

interface SlideProps {
  item: {};
}

export const BannerSlide = ({ item }: SlideProps) => {
  // const { movie_data } = item;

  return (
    <div className={styles.item}>
      {/* <Link href={paths.movie(item?.id)} className={styles.link} /> */}
      <div className={styles.content}>
        {/* <Title className={styles.title} as="h2" size="small">
          {movie_data?.name}
        </Title> */}

        <div className={styles.bottom}>
          {/* <MovieRating className={styles.rating}>{movie_data?.rating_imdb}</MovieRating> */}
          {/* <span className={styles.year}>{movie_data?.year}</span> */}
          {/* <span className={styles.genre}>{movie_data?.alternative_name}</span> */}
        </div>
      </div>
      {/* {!!movie_data?.preview_url && (
        <Image
          priority
          sizes="100%"
          fill
          quality={100}
          className={styles.image}
          src={movie_data?.preview_url}
          alt={movie_data?.name || ''}
        />
      )} */}
    </div>
  );
};
