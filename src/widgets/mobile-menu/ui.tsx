import { Button, Icon } from 'shared/ui';
import styles from './styles.module.scss';
import { useRouter } from 'next/router';
import { useToggler } from 'shared/lib';
import { searchModel } from 'entities/search-window';

export function MobileMenu() {
  const { push } = useRouter();
  const { open } = useToggler(searchModel.toggler);
  const items = [
    {
      title: 'Главная',
      icon: <Icon name="home" type="common" />,
      onClick: () => push('/'),
    },
    {
      title: 'Поиск',
      icon: <Icon name="search" type="common" />,
      onClick: () => open(),
    },
    {
      title: 'Списки',
      icon: <Icon name="bookmark" type="common" />,
      onClick: () => {},
    },
    {
      title: 'Профиль',
      icon: <Icon name="profile" type="common" />,
      onClick: () => {},
    },
  ];

  return (
    <div className={`${styles.mainWrapper} container`}>
      {items.map(({ title, icon, onClick }) => (
        <Button key={title} onClick={onClick}>
          <div className={styles.item}>
            {icon}
            <p>{title}</p>
          </div>
        </Button>
      ))}
    </div>
  );
}
