import { paths } from 'shared/routing';

export const colors = {
  [paths.home]: '#1a1a1a',
  [paths.anime]: '#051B22',
  [paths.series]: '#1a1a1a',
  [paths.doramas]: '#1B1A23',
  [paths.cartoons]: '#2A0944',
  [paths.films]: '#0F0E0E',
  [paths.kyrgyz]: '#1A132F',
  ['/series/[id]']: '#1A1A1A',
};
