import { useRouter } from 'next/router';
import { PropsWithChildren } from 'react';
import { colors } from './config';
import styles from './styles.module.scss';

export function PageBg({ children }: PropsWithChildren) {
  const router = useRouter();

  return (
    <div className={styles.pageBgWrapper} style={{ background: colors[router.pathname || ''] }}>
      {children}
    </div>
  );
}
