/* eslint-disable boundaries/element-types */
import { usePageEvent } from 'nextjs-effector';
import type { PropsWithChildren } from 'react';
import { AuthWindow } from 'widgets/auth';
import { Header } from 'widgets/header';
import { SearchWindow } from 'entities/search-window';
import { appStarted } from 'shared/config';

// run process logic for all base layout pages
import 'processes/root';
import { PageBg } from './page-bg';
import Script from 'next/script';
import { MobileMenu } from 'widgets/mobile-menu';

export const BaseLayout = ({ children }: PropsWithChildren) => {
  usePageEvent(appStarted);

  return (
    <PageBg>
      <Script
        type="text/javascript"
        dangerouslySetInnerHTML={{
          __html: `
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(${process.env.YM_TOKEN}, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
      `,
        }}
      />
      <Script strategy="afterInteractive" src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GA_TOKEN}`} />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '${process.env.GA_TOKEN}');
          `}
      </Script>
      <Header />
      <main className="main">{children}</main>
      <MobileMenu />
      <SearchWindow />
      <AuthWindow />
    </PageBg>
  );
};
