import clsx from 'clsx';
import { useEvent, useStore } from 'effector-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { filtersModel } from 'features/filters';
import { useLockedBody, useToggler } from 'shared/lib';
import { Button, Title } from 'shared/ui';
import { Icon } from 'shared/ui/icon';
import { sort, filters } from '../config';
import { DrawerSelect } from './drawer-select';
import { Select } from './select';
import styles from './styles.module.scss';
import { genresModel } from 'shared/genres';
import { genresToSelectOptions } from '../lib';
import { useEffect } from 'react';
import { MovieTypeIDEnum } from 'shared/movies';

interface Filters {
  typeOfMovieId: MovieTypeIDEnum;
}

export const Filters = ({ typeOfMovieId }: Filters) => {
  const { query } = useRouter();
  const genres = useStore(filtersModel.$genres);
  const { isOpen, close } = useToggler(filtersModel.toggler);
  const optionSelected = useEvent(filtersModel.optionSelected);
  const selectOpened = useEvent(genresModel.getGenres);
  const getGenres = useEvent(filtersModel.events.filtersStarted);
  const showResults = useEvent(filtersModel.showResults);
  const sendOption = useEvent(filtersModel.sendOption);
  useLockedBody(isOpen);

  useEffect(() => {
    if (typeOfMovieId) {
      getGenres(typeOfMovieId);
    }
  }, [typeOfMovieId]);

  /* добавить кнопку сбросить */
  return (
    <div className={clsx(styles.root, isOpen && styles.isOpen)}>
      <div className={styles.header}>
        <div className={styles.logo}>
          <Image priority className={styles.image} width={131} height={26} src="/logo.svg" alt="MyShows" />
        </div>
        <button onClick={close} className={clsx('btn-reset', styles.close)}>
          <Icon type="common" name="close" />
        </button>
      </div>
      <div className={styles.container}>
        <div className={styles.top}>
          <Title size="medium">Фильтры</Title>
        </div>
        <div className={styles.filters}>
          <div className={styles.row}>
            <Select
              value={query.genres}
              onSelect={({ value }) => optionSelected({ genres: value })}
              className={styles.select}
              label={'Жанры'}
              options={genresToSelectOptions(genres?.results || null)}
              onOpen={() => !genres?.results?.length && selectOpened()}
            />

            {filters.map(({ queryName, ...rest }) => (
              <Select
                key={queryName}
                value={query[queryName]}
                onSelect={({ value }) => optionSelected({ [queryName]: value })}
                className={styles.select}
                {...rest}
              />
            ))}
          </div>
          <div className={styles.row}>
            <Select
              value={query.ordering}
              onSelect={(option) => optionSelected({ ordering: option.value })}
              options={sort}
              startIcon={<Icon type="common" name="sort" />}
              placement="bottom-end"
              className={styles.select}
              label="Рекомендуемые"
            />
          </div>
        </div>
        <div className={styles.options}>
          <DrawerSelect
            value={query.ordering}
            onSelect={(option) => sendOption({ ordering: option.value })}
            options={sort}
            label="Сортировка"
          />
          {filters.map(({ queryName, ...rest }) => (
            <DrawerSelect
              key={queryName}
              value={query[queryName]}
              onSelect={({ value }) => sendOption({ [queryName]: value })}
              {...rest}
            />
          ))}
        </div>
        <Button onClick={showResults} className={styles.btn} variant="primary">
          Показать результаты
        </Button>
      </div>
    </div>
  );
};
