import { SortEnum } from 'shared/config';
import type { SelectOption } from '../types';
import { filtersModel } from '..';

export const sort: SelectOption[] = [
  { value: '', label: 'Рекомендуемые' },
  { value: SortEnum.RatingAscending, label: 'Возрастание рейтинга' },
  { value: SortEnum.RatingDescending, label: 'Убывание рейтинга' },
  { value: SortEnum.YearDescending, label: 'Сначала новые' },
  { value: SortEnum.YearAscending, label: 'Сначала старые' },
];

export const ratings: SelectOption[] = [
  { value: '', label: 'Любой рейтинг' },
  { value: '9', label: 'Больше 9' },
  { value: '8', label: 'Больше 8' },
  { value: '7', label: 'Больше 7' },
  { value: '6', label: 'Больше 6' },
  { value: '5', label: 'Больше 5' },
  { value: '4', label: 'Больше 4' },
  { value: '3', label: 'Больше 3' },
];

export const years: SelectOption[] = [
  { value: '', label: 'Все годы' },
  { value: '2023-2024', label: '2023 – 2024' },
  { value: '2022-2023', label: '2022 – 2023' },
  { value: '2020-2021', label: '2020 – 2021' },
  { value: '2014-2019', label: '2014 – 2019' },
  { value: '2010-2014', label: '2010 – 2014' },
  { value: '2000-2009', label: '2000 – 2009' },
  { value: '1990-1999', label: '1990 – 1999' },
  { value: '1980-1989', label: '1980 – 1989' },
  { value: '1970-1979', label: '1970 – 1979' },
  { value: '1960-1969', label: '1960 – 1969' },
  { value: '0-1959', label: 'до 1959' },
];

const genresStore = filtersModel?.$genres?.getState();

export const genres = genresStore && genresStore.results.map(({ id, name }) => ({ value: id, label: name }));

export const filters = [
  { label: 'Рейтинг', queryName: 'imdb_rating__gte', options: ratings },
  { label: 'Годы выхода', queryName: 'year', options: years },
];
