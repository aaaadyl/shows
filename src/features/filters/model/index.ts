import type { ParsedUrlQuery } from 'querystring';
import { attach, createEvent, createStore, restore, sample } from 'effector';
import { createToggler, paramsToString } from 'shared/lib';
import { navigationModel } from 'shared/navigation';
import { years } from '../config';
import { getOption } from '../lib';
import { commonApi } from 'shared/api';
import { MovieTypeIDEnum } from 'shared/movies';

export const toggler = createToggler();

export const $params = createStore('');

const $filters = createStore<Array<string | undefined>>([]);

// applies types_of_movie.id to fetch genres by type of movie (anime, doramy, series etc.)
const filtersStarted = createEvent<MovieTypeIDEnum>();

export const getCountriesFx = attach({ effect: commonApi.common.commonCountriesList });

export const getGenresFx = attach({
  effect: commonApi.common.commonGenresList,
  mapParams: (typeId) => ({ type_obj: typeId }),
});

export const $genres = restore(getGenresFx, null);

export const $countries = restore(getCountriesFx, null);

filtersStarted.watch((typeOfMovie) => {
  getGenresFx(typeOfMovie);
  switch (typeOfMovie) {
    case MovieTypeIDEnum.Cartoons:
    case MovieTypeIDEnum.Film:
    case MovieTypeIDEnum.Series:
      getCountriesFx({});
      break;
    default:
      break;
  }
});

sample({
  clock: navigationModel.$query,
  filter: Boolean,
  fn: ({ genre, year }) => [`${genre}`, getOption(years, year as string)],
  target: $filters,
});

sample({
  clock: navigationModel.$query,
  source: $filters,
  fn: (params) => paramsToString(params as string[]),
  target: $params,
});

export const optionSelected = createEvent<ParsedUrlQuery>();

export const sendOption = createEvent<ParsedUrlQuery>();

export const $allParams = createStore<ParsedUrlQuery | null>(null).on(sendOption, (state, payload) => ({
  ...state,
  ...payload,
}));

export const showResults = createEvent();

/* Передаем все параметры в query только когда вызвался эвент showResults */

sample({
  clock: showResults,
  source: $allParams,
  target: [navigationModel.pushQueryFx, toggler.close],
});

/* Когда что-то выбрали в селекте, сразу напрямую передаём в query */

sample({
  clock: optionSelected,
  target: navigationModel.pushQueryFx,
});

export const events = { filtersStarted };
