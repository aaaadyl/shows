import type { SelectOption } from 'features/filters';
import { Genre } from 'shared/api';

export function genresToSelectOptions(genres: Genre[] | null): SelectOption[] | null {
  const options = !!genres?.length && genres.map(({ id, name }) => ({ value: `${id}`, label: name }));
  const filterOptions = options;
  return filterOptions ? [{ value: '', label: 'Все' }, ...filterOptions] : null;
}
