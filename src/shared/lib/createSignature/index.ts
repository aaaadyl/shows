import { HmacSHA256, enc } from 'crypto-js';

export function createSignature(privateKey: string, data: string) {
  const signature = HmacSHA256(data, privateKey);
  return enc.Hex.stringify(signature);
}
