export const getRating = (rating: number | null | undefined) => (rating ?? 0)?.toFixed(1);
