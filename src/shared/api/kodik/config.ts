import axios from 'axios';
import { createHttp } from 'effector-http-api';

export const kodikInstance = axios.create({
  baseURL: process.env.API_KODIK_URL,
});

export const http = createHttp(kodikInstance);
