export interface KodikBResponse {
  links: Record<
    string,
    {
      Src: string;
      Type: string;
    }
  >;
}

export interface KodikSeason {
  link: string;
  episodes: Record<string, string>;
}

export interface KodikTranslations {
  _id: string;
  translate: string;
  last_season: number;
  last_episode: number;
  episodes_count: number;
  guality: string;
  seasons: Record<string, KodikSeason>;
}
