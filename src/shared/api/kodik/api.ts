import { utcToZonedTime } from 'date-fns-tz';
import { http } from './config';
import { addHours, format } from 'date-fns';
import { createSignature } from 'shared/lib';
import { KodikBResponse } from './types';

const routesConfig = http.createRoutesConfig({
  getMoviePlayer: http.createRoute<{ link: string; ip: string }, KodikBResponse>(({ link, ip }) => {
    const utcDate = utcToZonedTime(new Date(), 'UTC');
    const deadlineUTC = addHours(utcDate, 5);
    const deadline = format(deadlineUTC, 'yyyyMMddHH');
    const prKey = process.env.API_KODIK_PRIVATE_TOKEN as string;
    const sign = createSignature(prKey, `${link}:${ip}:${deadline}`);

    return {
      url: `/video-links`,
      params: {
        link,
        // p: process.env.API_KODIK_PUBLIC_TOKEN,
        ip,
        d: deadline,
        s: sign,
      },
    };
  }),
});

export const kodikApi = routesConfig.build();
