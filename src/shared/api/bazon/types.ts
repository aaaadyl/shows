export interface BazonMovieQuality {
  [quality: string]: string;
}

export type BazonMovieEpisodes = Record<string, string>;

export type BazonMovieSeason = Record<string, Record<string, Record<string, string>>>;

export interface BazonTranslation {
  playlists: BazonMovieEpisodes | BazonMovieSeason;
  poster: string;
  kinopoisk_id: string;
  date: string;
  camrip: string;
  ads: string;
  serial: string;
  quality: string;
  translation: string;
  studio: string;
}

export interface BazonPlayerMovie {
  results: BazonTranslation[];
}
