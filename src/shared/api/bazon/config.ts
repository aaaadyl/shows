import axios from 'axios';
import { createHttp } from 'effector-http-api';

export const bazonInstance = axios.create({
  baseURL: process.env.API_BN_URL,
  params: {
    token: process.env.API_BN_TOKEN,
  },
});

export const http = createHttp(bazonInstance);
