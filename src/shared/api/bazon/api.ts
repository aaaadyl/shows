import { http } from './config';
import type { BazonPlayerMovie } from './types';

const routesConfig = http.createRoutesConfig({
  getMoviePlayer: http.createRoute<{ kp: string; ip: string }, BazonPlayerMovie>(({ kp, ip }) => ({
    url: `/playlist?kp=${kp}&ip=${ip}`,
  })),
});

export const bazonApi = routesConfig.build();
