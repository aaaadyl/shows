import axios from 'axios';
import { createHttp } from 'effector-http-api';
import * as qs from 'qs';

export const commonInstance = axios.create({
  baseURL: process.env.API_URL,
  paramsSerializer: function (params) {
    return qs.stringify(params, { arrayFormat: 'repeat' });
  },
  headers: {
    'X-API-KEY': process.env.API_KP_TOKEN,
  },
});

export const http = createHttp(commonInstance);
