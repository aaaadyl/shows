import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';

/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Country {
  /** ID */
  id?: number;
  /**
   * Name of country
   * @minLength 1
   * @maxLength 255
   */
  name: string;
}

export interface Genre {
  /** ID */
  id?: number;
  /**
   * Name of genre
   * @minLength 1
   * @maxLength 255
   */
  name: string;
  /**
   * Slug
   * @format slug
   * @minLength 1
   * @pattern ^[-a-zA-Z0-9_]+$
   */
  slug?: string;
  /** Main Genres */
  main?: boolean;
  /**
   * Logo
   * @format uri
   */
  logo?: string | null;
  /** Type obj */
  type_obj?: string;
}

export interface TypeOfMovie {
  /** ID */
  id?: number;
  /**
   * Type of movie
   * @minLength 1
   * @maxLength 255
   */
  name: string;
}

export interface MovieSearchOptions {
  text: string;
  _index: string;
  _type: string;
  _id: string;
  _score: number;
  _source: {
    title: string;
    slug: string;
    poster_url: string;
    type_of_movie: {
      id: MovieTypeIDEnum;
      name: MovieTypeNameEnum;
    };
    year: number;
    imdb_rating: number;
  };
}

export interface MovieDocument {
  /**
   * Title
   * @minLength 1
   */

  text: string;
  offset: number;
  length: number;
  options: MovieSearchOptions[];
}

export interface MovieList {
  /** ID */
  id?: number;
  /**
   * Title
   * @minLength 1
   * @maxLength 255
   */
  title: string;
  /**
   * Slug
   * @format slug
   * @minLength 1
   * @pattern ^[-a-zA-Z0-9_]+$
   */
  slug?: string;
  /**
   * Poster url
   * @format uri
   */
  poster_url?: string | null;
  /**
   * Year
   * @min 0
   * @max 2147483647
   */
  year?: number | null;
  /**
   * Data from
   * @maxLength 255
   */
  data_from?: string | null;
  /** Description */
  description?: string | null;
  /** Kinopoisk rating */
  kinopoisk_rating?: number | null;
  /**
   * Kinopoisk votes
   * @min 0
   * @max 2147483647
   */
  kinopoisk_votes?: number | null;
  /** Imdb rating */
  imdb_rating?: number | null;
  /**
   * Imdb votes
   * @min 0
   * @max 2147483647
   */
  imdb_votes?: number | null;
  /**
   * Premiere
   * @format date-time
   */
  premiere?: string | null;
  /**
   * Age Rating
   * @maxLength 255
   */
  age_rating?: string | null;
  /** Type of movie */
  type_of_movie?: string;
}

export interface MovieDetail {
  /** ID */
  id?: number;
  /**
   * Title
   * @minLength 1
   * @maxLength 255
   */
  title: string;
  /**
   * Slug
   * @format slug
   * @minLength 1
   * @pattern ^[-a-zA-Z0-9_]+$
   */
  slug?: string;
  /**
   * Data from
   * @maxLength 255
   */
  data_from?: string | null;
  /**
   * Kinopoisk Id
   * @min 0
   * @max 2147483647
   */
  kinopoisk_id?: number | null;
  /**
   * Poster url
   * @format uri
   */
  poster_url?: string | null;
  /**
   * Year
   * @min 0
   * @max 2147483647
   */
  year?: number | null;
  /** Description */
  description?: string | null;
  /** Kinopoisk rating */
  kinopoisk_rating?: number | null;
  /**
   * Kinopoisk votes
   * @min 0
   * @max 2147483647
   */
  kinopoisk_votes?: number | null;
  /** Imdb rating */
  imdb_rating?: number | null;
  /**
   * Imdb votes
   * @min 0
   * @max 2147483647
   */
  imdb_votes?: number | null;
  /**
   * Premiere
   * @format date-time
   */
  premiere?: string | null;
  /**
   * Age Rating
   * @maxLength 255
   */
  age_rating?: string | null;
  /** Type of movie */
  type_of_movie?: string;
  countries?: Country[];
  genres?: Genre[];
  /** Get translation */
  get_translation?: KodikTranslations[];
}

export interface Season {
  /** ID */
  id?: number;
  /**
   * Title
   * @minLength 1
   * @maxLength 255
   */
  title: string;
  /**
   * Slug
   * @format slug
   * @minLength 1
   * @pattern ^[-a-zA-Z0-9_]+$
   */
  slug?: string;
  /**
   * Poster url
   * @format uri
   */
  poster_url?: string | null;
  /**
   * Data from
   * @maxLength 255
   */
  data_from?: string | null;
  /**
   * Year
   * @min 0
   * @max 2147483647
   */
  year?: number | null;
  /** Description */
  description?: string | null;
  /** Kinopoisk rating */
  kinopoisk_rating?: number | null;
  /**
   * Kinopoisk votes
   * @min 0
   * @max 2147483647
   */
  kinopoisk_votes?: number | null;
  /** Imdb rating */
  imdb_rating?: number | null;
  /**
   * Imdb votes
   * @min 0
   * @max 2147483647
   */
  imdb_votes?: number | null;
  /**
   * Premiere
   * @format date-time
   */
  premiere?: string | null;
  /**
   * Age Rating
   * @maxLength 255
   */
  age_rating?: string | null;
  /** Type of movie */
  type_of_movie?: string;
  /** Get translation */
  get_translation?: KodikTranslations[];
}

import { KodikBResponse, KodikTranslations } from '../kodik';
import { http } from './config';

/**
 * @title Shows API
 * @version v1
 * @license BSD License
 * @termsOfService https://www.google.com/policies/terms/
 * @baseUrl http://api-prod-shows.kg/api/v1
 * @contact <nursultandev@gmail.com>
 *
 * API for Shows
 */

const routesConfig = http.createRoutesConfig({
  common: {
    /**
     * @description ViewSet for Country model.
     *
     * @tags common
     * @name CommonCountriesList
     * @request GET:/common/countries/
     * @secure
     */
    commonCountriesList: http.createRoute<
      {
        /** Number of results to return per page. */
        limit?: number;
        /** The initial index from which to return the results. */
        offset?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: Country[];
      }
    >({
      url: `/common/countries/`,
      method: 'GET',
    }),

    /**
     * @description ViewSet for Country model.
     *
     * @tags common
     * @name CommonCountriesRead
     * @request GET:/common/countries/{id}/
     * @secure
     */
    commonCountriesRead: http.createRoute<number, Country>((id) => ({
      url: `/common/countries/${id}/`,
      method: 'GET',
    })),

    /**
     * @description ViewSet for Genre model.
     *
     * @tags common
     * @name CommonGenresList
     * @request GET:/common/genres/
     * @secure
     */
    commonGenresList: http.createRoute<
      {
        /** type_obj */
        type_obj?: string;
        /** Number of results to return per page. */
        limit?: number;
        /** The initial index from which to return the results. */
        offset?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: Genre[];
      }
    >({
      url: `/common/genres/`,
      method: 'GET',
    }),

    /**
     * @description ViewSet for Genre model.
     *
     * @tags common
     * @name CommonGenresRead
     * @request GET:/common/genres/{id}/
     * @secure
     */
    commonGenresRead: http.createRoute<number, Genre>((id) => ({
      url: `/common/genres/${id}/`,
      method: 'GET',
    })),

    /**
     * @description ViewSet for TypeOfMovie model.
     *
     * @tags common
     * @name CommonTypesList
     * @request GET:/common/types/
     * @secure
     */
    commonTypesList: http.createRoute<
      {
        /** Number of results to return per page. */
        limit?: number;
        /** The initial index from which to return the results. */
        offset?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: TypeOfMovie[];
      }
    >({
      url: `/common/types/`,
      method: 'GET',
    }),

    /**
     * @description ViewSet for TypeOfMovie model.
     *
     * @tags common
     * @name CommonTypesRead
     * @request GET:/common/types/{id}/
     * @secure
     */
    commonTypesRead: http.createRoute<number, TypeOfMovie>((id) => ({
      url: `/common/types/${id}/`,
      method: 'GET',
    })),
  },
  movies: {
    /**
     * No description
     *
     * @tags movies
     * @name MoviesKodicCreate
     * @summary Get Kodic Video Player
     * @request POST:/movies/kodic/
     * @secure
     */
    moviesKodicCreate: http.createRoute<
      {
        url?: string;
      },
      KodikBResponse
    >({
      url: `/movies/kodic/`,
      method: 'POST',
    }),

    /**
     * No description
     *
     * @tags movies
     * @name MoviesMovieSearchList
     * @request GET:/movies/movie-search/
     * @secure
     */
    moviesMovieSearchList: http.createRoute<
      {
        /** A page number within the paginated result set. */
        page?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: {
          title__completion: MovieDocument[];
        };
      }
    >({
      url: `/movies/movie-search/`,
      method: 'GET',
    }),

    /**
     * @description :param request: :return:
     *
     * @tags movies
     * @name MoviesMovieSearchFunctionalSuggest
     * @summary Functional suggest functionality.
     * @request GET:/movies/movie-search/functional_suggest/
     * @secure
     */
    moviesMovieSearchFunctionalSuggest: http.createRoute<
      {
        /** A page number within the paginated result set. */
        page?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: {
          title__completion: MovieDocument[];
        };
      }
    >({
      url: `/movies/movie-search/functional_suggest/`,
      method: 'GET',
    }),

    /**
     * @description Suggest functionality.
     *
     * @tags movies
     * @name MoviesMovieSearchSuggest
     * @request GET:/movies/movie-search/suggest/
     * @secure
     */
    moviesMovieSearchSuggest: http.createRoute<
      {
        /** A page number within the paginated result set. */
        title__completion: string;
        page?: number;
      },
      {
        title__completion: MovieDocument[];
      }
    >({
      url: `/movies/movie-search/suggest/`,
      method: 'GET',
    }),

    /**
     * No description
     *
     * @tags movies
     * @name MoviesMovieSearchRead
     * @request GET:/movies/movie-search/{id}/
     * @secure
     */
    moviesMovieSearchRead: http.createRoute<string, MovieDocument>((id) => ({
      url: `/movies/movie-search/${id}/`,
      method: 'GET',
    })),

    /**
     * @description ViewSet for Movie model.
     *
     * @tags movies
     * @name MoviesMoviesList
     * @request GET:/movies/movies/
     * @secure
     */
    moviesMoviesList: http.createRoute<
      {
        /** type_of_movie */
        type_of_movie?: string;
        /** year */
        year?: string;
        /** countries */
        countries?: string;
        /** imdb_rating__gte */
        imdb_rating__gte?: string;
        /** genres */
        genres?: string;
        /** Which field to use when ordering the results. */
        ordering?: string;
        /** Number of results to return per page. */
        limit?: number;
        /** The initial index from which to return the results. */
        offset?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: MovieList[];
      }
    >({
      url: `/movies/movies/`,
      method: 'GET',
    }),

    /**
     * @description ViewSet for Movie model.
     *
     * @tags movies
     * @name MoviesMoviesRead
     * @request GET:/movies/movies/{slug}/
     * @secure
     */
    moviesMoviesRead: http.createRoute<string, MovieDetail>((slug) => ({
      url: `/movies/movies/${slug}/`,
      method: 'GET',
    })),

    /**
     * @description ViewSet for Season model.
     *
     * @tags movies
     * @name MoviesSeasonsList
     * @request GET:/movies/seasons/
     * @secure
     */
    moviesSeasonsList: http.createRoute<
      {
        /** movie */
        movie?: string;
        /** A search term. */
        search?: string;
        /** Number of results to return per page. */
        limit?: number;
        /** The initial index from which to return the results. */
        offset?: number;
      },
      {
        count: number;
        /** @format uri */
        next?: string | null;
        /** @format uri */
        previous?: string | null;
        results: Season[];
      }
    >({
      url: `/movies/seasons/`,
      method: 'GET',
    }),

    /**
     * @description ViewSet for Season model.
     *
     * @tags movies
     * @name MoviesSeasonsRead
     * @request GET:/movies/seasons/{slug}/
     * @secure
     */
    moviesSeasonsRead: http.createRoute<string, Season>((slug) => ({
      url: `/movies/seasons/${slug}/`,
      method: 'GET',
    })),
  },
});

export const commonApi = routesConfig.build();
