export enum MovieTypeIDEnum {
  Film = 1,
  Anime = 2,
  Series = 3,
  Cartoons = 4,
  Dorama = 5,
}

export enum MovieTypeNameEnum {
  Film = 'movie',
  Anime = 'anime',
  Series = 'serial',
  Cartoons = 'cartoon',
  Dorama = 'doramy',
}
