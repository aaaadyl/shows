import { attach, createEvent, sample, restore } from 'effector';
import { commonApi } from 'shared/api';

export const getGenres = createEvent();

/* Атачнутые эффекты */
// export const getMoviesGenresFx = attach({ effect: commonApi.v1.v1MoviesGenresList });

export const $genres = null;

// sample({
//   clock: getGenres,
//   fn: () => ({ main: 'false', title: '' }),
//   target: getMoviesGenresFx,
// });
