export const enum MainGenresEnum {
  Films = 'Фильмы',
  Series = 'Сериалы',
  Cartoons = 'Мультфильмы',
  Doramas = 'Дорамы',
  Anime = 'Аниме',
}
