export interface SpritesMap {
  common:
    | 'bookmark'
    | 'burger'
    | 'check'
    | 'chevron'
    | 'close'
    | 'edit'
    | 'filters'
    | 'home'
    | 'link'
    | 'play'
    | 'profile'
    | 'search'
    | 'share'
    | 'sort'
    | 'star'
    | 'volume-high'
    | 'volume-slash';
  genres: 'cup' | 'fire' | 'happy' | 'hearts' | 'knife' | 'map' | 'music' | 'people' | 'rocket' | 'tank';
  player:
    | 'forward-10'
    | 'maximize'
    | 'minimize'
    | 'mute'
    | 'next'
    | 'pause'
    | 'pic-in-pic'
    | 'play'
    | 'replay-10'
    | 'settings'
    | 'settings2'
    | 'speed'
    | 'volume';
  social: 'facebook' | 'gmail' | 'telegram' | 'viber' | 'vk' | 'whatsapp';
}

export const SPRITES_META: { [K in keyof SpritesMap]: SpritesMap[K][] } = {
  common: [
    'bookmark',
    'burger',
    'check',
    'chevron',
    'close',
    'edit',
    'filters',
    'home',
    'link',
    'play',
    'profile',
    'search',
    'share',
    'sort',
    'star',
    'volume-high',
    'volume-slash',
  ],
  genres: ['cup', 'fire', 'happy', 'hearts', 'knife', 'map', 'music', 'people', 'rocket', 'tank'],
  player: [
    'forward-10',
    'maximize',
    'minimize',
    'mute',
    'next',
    'pause',
    'pic-in-pic',
    'play',
    'replay-10',
    'settings',
    'settings2',
    'speed',
    'volume',
  ],
  social: ['facebook', 'gmail', 'telegram', 'viber', 'vk', 'whatsapp'],
};
