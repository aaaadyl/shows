export enum RatingsEnum {
  imdb = 'IMDb',
  kp = 'Кинопоиск',
  shikimori = 'Shikimori',
}

export enum UnifiedMPAARating {
  G = 0,
  PG = 7,
  'PG-13' = 13,
  R = 17,
  'NC-17' = 18,
}
