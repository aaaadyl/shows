export const enum SortEnum {
  RatingAscending = 'imdb_rating',
  RatingDescending = '-imdb_rating',
  YearAscending = 'year',
  YearDescending = '-year',
}
