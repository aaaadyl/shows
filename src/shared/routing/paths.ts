import { MovieTypeNameEnum } from 'shared/movies';

interface CatalogParams {
  genres?: string;
  rating?: string;
  year?: string;
  sort?: string;
}

export type MovieType =
  | MovieTypeNameEnum.Anime
  | MovieTypeNameEnum.Film
  | MovieTypeNameEnum.Series
  | MovieTypeNameEnum.Dorama
  | MovieTypeNameEnum.Cartoons;

type Id = number | string | null | undefined;

export const paths = {
  home: '/',
  films: '/movie',
  series: '/serial',
  cartoons: '/cartoon',
  policy: '/policy',
  profile: '/profile',
  favorites: '/profile/favorites',
  history: '/profile/history',
  anime: '/anime',
  doramas: '/doramy',
  kyrgyz: '/kyrgyz',

  // Get the URL for the movie catalog page
  catalog: (params: CatalogParams, type: MovieTypeNameEnum): string => {
    const searchParams = new URLSearchParams({ ...params });
    const url = `/${type}?${searchParams}`;

    return url;
  },

  // Get the URL for the movie page
  movie: (id: Id, type: MovieType): string => `/${type}/${id}`,

  // Get the URL for the person page
  person: (id: Id): string => `/name/${id}`,

  // Get the URL for the studio page
  studio: (id: Id): string => `/studio/${id}`,
};

export const main_genres_paths = {
  films: paths.films,
  series: paths.series,
  cartoons: paths.cartoons,
  anime: paths.anime,
  doramas: paths.doramas,
};
