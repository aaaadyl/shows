import { NextPage } from 'next';
import { Catalog } from 'widgets/catalog';
import { animeModel } from '.';
import { useEvent, useStore } from 'effector-react';
import { usePageEvent } from 'nextjs-effector';
import { MovieCard } from 'entities/movie';
import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';

export const AnimePage: NextPage = () => {
  const loadMore = useEvent(animeModel.loadMore);
  const hasMore = useStore(animeModel.$hasMore);
  const pending = useStore(animeModel.$pending);
  const anime = useStore(animeModel.$anime);
  usePageEvent(animeModel.pageStarted);

  return (
    <>
      <Catalog
        loadMore={loadMore}
        hasMore={hasMore}
        pending={pending}
        renderItems={(item) => <MovieCard key={item.id} item={item} type={MovieTypeNameEnum.Anime} />}
        movies={anime?.results || null}
        title="Аниме"
        typeOfMovieId={MovieTypeIDEnum.Anime}
      />
    </>
  );
};
