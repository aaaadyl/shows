export function getProfessions(professions: { title: string }[]) {
  return professions.map((item) => item?.title).join(', ');
}
