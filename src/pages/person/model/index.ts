import { attach, createEvent, restore, sample } from 'effector';
import type { PageContext } from 'nextjs-effector';
import { commonApi } from 'shared/api';

export const pageStarted = createEvent<PageContext>();

// const getPersonByIdFx = attach({ effect: commonApi.v1.v1PersonsPersonsRead });
// const getFilmographyByIdFx = attach({ effect: commonApi.v1.v1PersonsPersonsPersonInMovies });

// export const $person = restore(getPersonByIdFx, null);
// export const $filmography = restore(getFilmographyByIdFx, null);

// sample({
//   clock: pageStarted,
//   fn: ({ params }) => Number(params?.id),
//   target: [getPersonByIdFx, getFilmographyByIdFx],
// });
