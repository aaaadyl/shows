import { NextPage } from 'next';
import { Catalog } from 'widgets/catalog';
import { doramaModel } from '.';
import { useEvent, useStore } from 'effector-react';
import { MovieCard } from 'entities/movie';
import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';

export const DoramaPage: NextPage = () => {
  const loadMore = useEvent(doramaModel.loadMore);
  const hasMore = useStore(doramaModel.$hasMore);
  const pending = useStore(doramaModel.$pending);
  const doramas = useStore(doramaModel.$doramas);

  return (
    <>
      <Catalog
        loadMore={loadMore}
        hasMore={hasMore}
        pending={pending}
        renderItems={(item) => <MovieCard key={item.id} item={item} type={MovieTypeNameEnum.Dorama} />}
        movies={doramas?.results || null}
        title="Дорамы"
        typeOfMovieId={MovieTypeIDEnum.Dorama}
      />
    </>
  );
};
