import { combine, createEffect, createEvent, createStore, restore, sample } from 'effector';
import type { PageContext } from 'nextjs-effector';
import { commonApi } from 'shared/api';
import { MovieTypeIDEnum } from 'shared/movies';

export const pageStarted = createEvent<PageContext>();

export const getDoramasFx = createEffect(commonApi.movies.moviesMoviesList);
export const $doramas = restore(getDoramasFx, null);

export const loadMore = createEvent();

export const $hasMore = createStore(false);

export const $limit = createStore(30)
  .on(loadMore, (state) => state + 60)
  .reset(pageStarted);

const $pageContext = createStore<PageContext | null>(null);

sample({
  clock: pageStarted,
  target: $pageContext,
});

const $params = combine({ context: $pageContext, limit: $limit });

sample({
  clock: [pageStarted, loadMore],
  source: $params,
  fn: ({ limit, context }) => {
    // const ctxGenres = typeof context?.query?.genres === 'string' ? context?.query?.genres : context?.query?.genres?.[0];

    return {
      ...context?.query,
      limit,
      type_of_movie: String(MovieTypeIDEnum.Dorama),
      // genres: ctxGenres ? ['23', ctxGenres] : '23',
    };
  },
  target: getDoramasFx,
});

export const $pending = getDoramasFx.pending;

sample({
  clock: getDoramasFx.doneData,
  source: $limit,
  fn: (limit, options) => {
    return limit < options?.count;
  },
  target: $hasMore,
});
