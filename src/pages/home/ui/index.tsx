import { ComedyFilms } from './comedy-films';
import { Drama } from './drama';
import { Fantastic } from './fantastic';
import { ForFamily } from './for-family';
import { Info } from './info';
import { NewFilms } from './new-films';

export const HomePage = () => (
  <div>
    <ComedyFilms />
    <NewFilms />
    <ForFamily />
    <Drama />
    <Fantastic />
    <Info />
  </div>
);
