import { attach, combine, createEvent, restore, sample } from 'effector';
import { commonApi } from 'shared/api';
import { appStarted } from 'shared/config';

export const pageStarted = createEvent();

export const $offset = 10;
export const $limit = 10;

const $params = combine({ offset: $offset, limit: $limit });

// sample({
//   clock: appStarted,
//   source: $params,
//   target: [getNewMoviesFx, getComedyMoviesFx, getFamilyMoviesFx, getDramaMoviesFx, getFantasticMoviesFx],
// });
