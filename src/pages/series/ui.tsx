import { useEvent, useStore } from 'effector-react';
import { NextPage } from 'next';
import { Catalog } from 'widgets/catalog';
import { seriesModel } from '.';
import { MovieCard } from 'entities/movie';
import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';

export const SeriesPage: NextPage = () => {
  const loadMore = useEvent(seriesModel.loadMore);
  const hasMore = useStore(seriesModel.$hasMore);
  const pending = useStore(seriesModel.$pending);
  const series = useStore(seriesModel.$series);

  return (
    <>
      <Catalog
        movies={series?.results || null}
        loadMore={loadMore}
        hasMore={hasMore}
        pending={pending}
        title="Сериалы"
        renderItems={(item) => <MovieCard key={item.id} item={item} type={MovieTypeNameEnum.Series} />}
        isSeries
        typeOfMovieId={MovieTypeIDEnum.Series}
      />
    </>
  );
};
