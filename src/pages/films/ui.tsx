import { useEvent, useStore } from 'effector-react';
import type { NextPage } from 'next';
import { Catalog } from 'widgets/catalog';
import { filmsModel } from '.';
import { MovieCard } from 'entities/movie';
import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';
// import { bannerModel } from 'widgets/banner';
// import { useEffect } from 'react';

export const FilmsPage: NextPage = () => {
  const loadMore = useEvent(filmsModel.loadMore);
  const hasMore = useStore(filmsModel.$hasMore);
  const pending = useStore(filmsModel.$pending);
  const films = useStore(filmsModel.$films);

  // const getBannerMovies = useEvent(bannerModel.getBanners);

  // useEffect(() => {
  //   getBannerMovies('anime');
  // }, []);

  return (
    <>
      {/* <Banner /> */}
      <Catalog
        loadMore={loadMore}
        hasMore={hasMore}
        pending={pending}
        movies={films?.results || null}
        renderItems={(item) => <MovieCard key={item.id} item={item} type={MovieTypeNameEnum.Film} />}
        title="Фильмы"
        typeOfMovieId={MovieTypeIDEnum.Film}
      />
    </>
  );
};
