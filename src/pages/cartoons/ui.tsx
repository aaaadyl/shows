import { useEvent, useStore } from 'effector-react';
import type { NextPage } from 'next';
import { Catalog } from 'widgets/catalog';
import { cartoonsModel } from '.';
import { MovieCard } from 'entities/movie';
import { MovieTypeIDEnum, MovieTypeNameEnum } from 'shared/movies';

export const CartoonsPage: NextPage = () => {
  const loadMore = useEvent(cartoonsModel.loadMore);
  const hasMore = useStore(cartoonsModel.$hasMore);
  const pending = useStore(cartoonsModel.$pending);
  const cartoons = useStore(cartoonsModel.$cartoons);
  return (
    <>
      <Catalog
        loadMore={loadMore}
        hasMore={hasMore}
        pending={pending}
        renderItems={(item) => <MovieCard key={item.id} item={item} type={MovieTypeNameEnum.Cartoons} />}
        movies={cartoons?.results || null}
        typeOfMovieId={MovieTypeIDEnum.Cartoons}
        title="Мультфильмы"
      />
    </>
  );
};
