import { combine, createEffect, createEvent, createStore, restore, sample } from 'effector';
import type { PageContext } from 'nextjs-effector';
import { commonApi } from 'shared/api';
import { MovieTypeIDEnum } from 'shared/movies';

export const pageStarted = createEvent<PageContext>();

export const getCartoonsFx = createEffect(commonApi.movies.moviesMoviesList);
export const $cartoons = restore(getCartoonsFx, null);

export const loadMore = createEvent();

export const $hasMore = createStore(false);

export const $limit = createStore(30)
  .on(loadMore, (state) => state + 60)
  .reset(pageStarted);

const $pageContext = createStore<PageContext | null>(null);

sample({
  clock: pageStarted,
  target: $pageContext,
});

const $params = combine({ context: $pageContext, limit: $limit });

sample({
  clock: [pageStarted, loadMore],
  source: $params,
  fn: ({ limit, context }) => {
    // const ctxGenres = typeof context?.query?.genres === 'string' ? context?.query?.genres : context?.query?.genres?.[0];
    return { ...context?.query, limit, type_of_movie: String(MovieTypeIDEnum.Cartoons) };
  },
  target: getCartoonsFx,
});

export const $pending = getCartoonsFx.pending;

sample({
  clock: getCartoonsFx.doneData,
  source: $limit,
  fn: (limit, options) => {
    return limit < options?.count;
  },
  target: $hasMore,
});
