import { NextSeo, DefaultSeo } from 'next-seo';
import { ComponentType } from 'react';

// eslint-disable-next-line operator-linebreak
export const withSeo =
  <P extends object>(Component: ComponentType<P>) =>
  (props: P) =>
    (
      <>
        <NextSeo
          nofollow
          noindex
          titleTemplate="%s | SHOWS"
          description="Устройте кинотеатр у себя дома! Смотрите онлайн фильмы хорошего качества в приятной домашней обстановке и в удобное для вас время. Для вас всегда доступны на любой вкус: сериалы, фильмы, мультфильмы и многое другое."
          openGraph={{
            title: 'MyShows - фильмы и сериалы',
            description: 'MyShows - фильмы и сериалы',
          }}
          additionalMetaTags={[
            {
              name: 'keywords',
              content:
                'фильмы онлайн в хорошем отличном качестве без смс кино видео смотреть новинки кинофильмы онлайн кинотеатр 2020 2021 2022 просмотр видеоролики',
            },
            {
              name: 'viewport',
              content: 'width=device-width,initial-scale=1',
            },
            {
              name: 'apple-mobile-web-app-capable',
              content: 'yes',
            },
            {
              name: 'google-site-verification',
              content: 'google-site-verification=hRmAjA3eZyyNzNLEmdq8WP1kRtJX9v4MvnUx4BZAJfc', //hRmAjA3eZyyNzNLEmdq8WP1kRtJX9v4MvnUx4BZAJfc
            },
            {
              name: 'yandex-verification',
              content: '350cb8beaf80b137',
            },
          ]}
          additionalLinkTags={[
            {
              rel: 'icon',
              href: '/favicon-package/favicon.ico',
            },
          ]}
        ></NextSeo>
        <Component {...props} />
      </>
    );
