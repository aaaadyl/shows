import Image from 'next/image';
import Link from 'next/link';
import { getRating, minutesToHour } from 'shared/lib';
import { MovieType, paths } from 'shared/routing';
import { MovieRating } from 'shared/ui/movie-rating';
import styles from './styles.module.scss';
import { MovieList } from 'shared/api';
import { MovieTypeNameEnum } from 'shared/movies';

interface MovieCardProps {
  item: Partial<MovieList>;
  rating?: number | null;
  small?: boolean;
  type?: MovieType;
}

export const MovieCard = ({ item, rating, small, type = MovieTypeNameEnum.Film }: MovieCardProps) => {
  const movieRating = Number(rating ?? getRating(item.imdb_rating)).toFixed(1);

  return (
    <Link className={styles.item} href={paths.movie(item?.slug, type)}>
      <div className={styles.imageWrapper}>
        {item?.id && (
          <Image
            sizes="100%"
            fill
            quality={100}
            className={styles.image}
            alt={item?.title ?? ''}
            src={item?.poster_url || ''}
          />
        )}
      </div>
      <div className={styles.content}>
        {movieRating ? (
          <MovieRating showState className={styles.rating}>
            {movieRating}
          </MovieRating>
        ) : null}
        <h3 className={styles.name}>{item?.title}</h3>
        {!small && (
          <div className={styles.info}>
            <span className={styles.year}>{item?.year}</span>
            {/* {item?.movie_length && <span className={styles.length}>{minutesToHour(+item.movie_length)}</span>} */}
          </div>
        )}
      </div>
    </Link>
  );
};
