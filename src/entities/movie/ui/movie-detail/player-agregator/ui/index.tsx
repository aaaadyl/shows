import { useWindowSize } from 'entities/movie/lib';
import styles from './styles.module.scss';
import { CSSProperties } from 'react';

interface Player {
  kinopoisk_id: string;
}

export const PlayerAgregator = ({ kinopoisk_id }: Player) => {
  const { height } = useWindowSize();

  return (
    <div className={`${styles.root} container`} style={{ height: `${height}px` } as CSSProperties}>
      <iframe
        src={`https://voidboost.tv/embed/${kinopoisk_id}?poster=1&poster_id=4&df=1`}
        allow="autoplay"
        className={styles.iframe}
        allowFullScreen
      />
    </div>
  );
};
