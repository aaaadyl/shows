import { useEvent, useStore } from 'effector-react';
import styles from './styles.module.scss';
import { Button } from 'shared/ui';
import { kodikAnimeModel } from 'entities/movie/model';

interface KodikMovieParams {
  movie_id: number;
}

export const KodikAnimeMovieParams = ({ movie_id }: KodikMovieParams) => {
  const state = useStore(kodikAnimeModel.$kodikAnimeMovieState);
  const selectAnimeSeason = useEvent(kodikAnimeModel.events.selectSeason);
  const selectTranslation = useEvent(kodikAnimeModel.events.selectTranslation);
  const selectSerie = useEvent(kodikAnimeModel.events.selectSerie);
  const series = Object.entries(state.series || {});

  return (
    <div className={`container ${styles.container}`}>
      <h3>Сезоны</h3>
      <div className={styles.trsContainer}>
        {!!state.seasons?.length &&
          state.seasons.sort().map((season) => {
            return (
              <Button
                key={season.title}
                onClick={() => {
                  selectAnimeSeason(season.title);
                }}
                disabled={state.selectedSeason === season.title}
                variant={state.selectedSeason === season.title ? 'primary' : 'glass'}
              >
                {season.title}
              </Button>
            );
          })}
      </div>

      <h3>Озвучки</h3>
      <div className={styles.trsContainer}>
        {!!state.translations?.length &&
          state.translations.map((translation) => {
            return (
              <Button
                key={translation._id}
                variant={String(translation._id) === String(state.selectedTranslation) ? 'primary' : 'glass'}
                disabled={String(translation._id) === String(state.selectedTranslation)}
                onClick={() => {
                  translation._id && selectTranslation(String(translation._id));
                }}
              >
                {translation.translate}
              </Button>
            );
          })}
      </div>

      <h3>Серии</h3>
      <div className={styles.trsContainer}>
        {!!series &&
          series.map(([serie, link]) => {
            return (
              <Button
                key={`${serie}_${link}`}
                onClick={() => selectSerie(serie)}
                disabled={String(state?.selectedSerie) === serie}
                variant={String(state?.selectedSerie) === serie ? 'primary' : 'glass'}
              >
                {serie}
              </Button>
            );
          })}
      </div>
    </div>
  );
};
