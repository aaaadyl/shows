import clsx from 'clsx';
import { useStore } from 'effector-react';
import styles from './styles.module.scss';
import { Title } from 'shared/ui';
import { commonMovieDetailModel } from 'entities/movie/model';
import { MovieTypeIDEnum } from 'shared/movies';

const movieRuTypeName = {
  [MovieTypeIDEnum.Anime]: 'это аниме',
  [MovieTypeIDEnum.Series]: 'этот сериял',
  [MovieTypeIDEnum.Film]: 'этот фильм',
  [MovieTypeIDEnum.Dorama]: 'эта дорама',
  [MovieTypeIDEnum.Cartoons]: 'этот мультфилм',
};

export const Description = () => {
  const data = useStore(commonMovieDetailModel.$movie);
  const text = data?.description ?? 'Без описания';
  const movieType = Number(data?.type_of_movie) as MovieTypeIDEnum;

  return (
    <section>
      <div className={clsx([styles.container, 'container'])}>
        <Title size="small">
          Про что {movieRuTypeName[movieType || MovieTypeIDEnum.Film]} «{data?.title || ''}»:
        </Title>
        <p className={styles.desc}>{text}</p>
      </div>
    </section>
  );
};
