import { useStore } from 'effector-react';
import { Category } from 'widgets/category';
import styles from './styles.module.scss';
import { commonMovieDetailModel } from 'entities/movie/model';

export const SimilarMovies = () => {
  const data = useStore(commonMovieDetailModel.$movie);

  // if (!data?.similarMovies?.length) return null;

  return (
    <Category containerClass={styles.container}>
      {/* <Category.Title className={styles.title}>Похожее</Category.Title> */}
      {/* <Category.Carousel items={data?.similarMovies} renderItem={(item) => <MovieCard item={item} />} /> */}
    </Category>
  );
};
