import { useEvent, useStore } from 'effector-react';
import styles from './styles.module.scss';
import { Button } from 'shared/ui';
import { BazonMoviesModel } from 'entities/movie/model';

interface BazonMovieParams {}

export const BazonMoviesParams = ({}: BazonMovieParams) => {
  const state = useStore(BazonMoviesModel.$bazonParamsState);
  // const selectAnimeSeason = useEvent(kodikDoramaModel.events.selectSeason);
  // const selectTranslation = useEvent(kodikDoramaModel.events.selectTranslation);
  // const selectSerie = useEvent(kodikDoramaModel.events.selectSerie);

  return (
    <div className={`container ${styles.container}`}>
      <h3>Bazon Озвучки</h3>
      <div className={styles.trsContainer}>
        {/* {!!state.translations?.length &&
          state.translations.map((translation) => {
            return (
              <Button
                key={translation._id}
                variant={String(translation._id) === String(state.selectedTranslation) ? 'primary' : 'glass'}
                disabled={String(translation._id) === String(state.selectedTranslation)}
                onClick={() => {
                  translation._id && selectTranslation(String(translation._id));
                }}
              >
                {translation.translate}
              </Button>
            );
          })} */}
      </div>

      {/* <h3>Сезоны</h3>
      <div className={styles.trsContainer}>
        {!!state.seasons?.length &&
          state.seasons.sort().map((title) => {
            return (
              <Button
                key={title}
                onClick={() => {
                  selectAnimeSeason(title);
                }}
                disabled={state.selectedSeason === title}
                variant={state.selectedSeason === title ? 'primary' : 'glass'}
              >
                {title}
              </Button>
            );
          })}
      </div>

      <h3>Dorama Series</h3>
      <div className={styles.trsContainer}>
        {!!state.series &&
          Object.entries(state.series).map(([serie, link]) => {
            return (
              <Button
                key={link}
                onClick={() => {
                  selectSerie(link);
                }}
                disabled={String(state?.selectedSerie) === link}
                variant={String(state?.selectedSerie) === link ? 'primary' : 'glass'}
              >
                {serie}
              </Button>
            );
          })}
      </div> */}
    </div>
  );
};
