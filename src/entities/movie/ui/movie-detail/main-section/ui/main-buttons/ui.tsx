// import clsx from 'clsx';
import { useStore } from 'effector-react';
// import { useRouter } from 'next/router';
// import { favoritesModel } from 'features/favorites';
import { useToggler } from 'shared/lib';
import { Button } from 'shared/ui/button';
import { Icon } from 'shared/ui/icon';
import styles from './styles.module.scss';
import { commonMovieDetailModel } from 'entities/movie/model';

export const MainButtons = () => {
  // const { query } = useRouter();
  const data = useStore(commonMovieDetailModel.$movie);
  // const trailerToggler = useToggler(commonMovieDetailModel.trailerToggler);
  const playerToggler = useToggler(commonMovieDetailModel.playerToggler);
  // const toggleFavorite = useEvent(favoritesModel.toggleFavorite);
  // const isFavorite = useStore(favoritesModel.$isFavorite);

  return (
    <div className={styles.btns}>
      {/* <Button onClick={playerToggler.open} size="big" className={styles.btn} gradient variant="primary">
        <Icon type="player" name="play" />
        Смотреть
      </Button> */}
      {/* {!!data?.trailers?.length && (
        <Button onClick={trailerToggler.open} size="big" className={styles.btn} variant="glass">
          Трейлер
        </Button>
      )} */}
      {/* <Button
        onClick={() => toggleFavorite({ id: query?.id as string })}
        size="big"
        className={clsx(styles.btn, isFavorite && styles.isFavorite)}
        variant="glass"
      >
        <Icon type="common" name="bookmark" />
      </Button> */}
    </div>
  );
};
