import { useStore } from 'effector-react';
import react, { useCallback } from 'react';
import { useToggler } from 'shared/lib/toggler';
import { Popup, Spinner } from 'shared/ui';
import styles from './styles.module.scss';
import { commonMovieDetailModel } from 'entities/movie/model';

export const TrailerModal = () => {
  const { close, isOpen } = useToggler(commonMovieDetailModel.trailerToggler);
  const [isLoading, setIsLoading] = react.useState<boolean>(true);
  const data = useStore(commonMovieDetailModel.$movie);
  const [currentUrlIndex, setCurrentUrlIndex] = react.useState<number>(0);

  const changeTrailerUrl = useCallback(() => {
    // setCurrentUrlIndex((prev) => ((data?.trailers?.length || 0) > prev ? prev + 1 : prev));
  }, [data]);

  return (
    <Popup className={styles.modal} isOpen={isOpen} close={close}>
      <div className={styles.content}>
        {/* {data?.trailers?.length ? (
          <>
            {isLoading && (
              <div className={styles.spinner}>
                <Spinner strokeWidth={2} />
              </div>
            )}
            <ReactPlayer
              url={data?.trailers?.[currentUrlIndex].url}
              playing
              onError={changeTrailerUrl}
              onReady={() => setIsLoading(false)}
              className={styles.iframe}
              width={'100%'}
              height={'100%'}
              controls
            />
          </>
        ) : null} */}
      </div>
      <Popup.Close onClick={close} className={styles.close} />
    </Popup>
  );
};
