import { format } from 'date-fns';
import styles from './styles.module.scss';
import { useStore } from 'effector-react';
import { commonMovieDetailModel } from 'entities/movie/model';
import { useRef } from 'react';
import { RatingsEnum, UnifiedMPAARating } from 'shared/config/ratings';
import { Link } from 'shared/ui';
import { paths } from 'shared/routing';
import { MovieTypeNameEnum } from 'shared/movies';

export const DetailFields = () => {
  const data = useStore(commonMovieDetailModel.$movie);
  const ratings = useStore(commonMovieDetailModel.$ratings);
  const premiere = data?.premiere && format(new Date(data?.premiere), 'yyyy.MM.dd');
  const ageRating = data?.age_rating as keyof typeof UnifiedMPAARating;

  const fields = useRef(
    [
      {
        label: <p className={styles.label}>Дата выхода:</p>,
        value: <p className={styles.value}>{premiere}</p>,
      },
      {
        label: <p className={styles.label}>Рейтинг:</p>,
        value: !!ratings?.length && (
          <div className={styles.valuesWrapper}>
            {ratings.map(({ votes, rating, name }) => (
              <p key={`${name}-${votes}`} className={styles.value}>
                {name}: <b>{rating}</b> <span>({votes})</span>
              </p>
            ))}
          </div>
        ),
      },
      {
        label: <p className={styles.label}>Страна:</p>,
        value: <p className={styles.value}>{data?.countries?.[0]?.name}</p>,
      },
      {
        label: <p className={styles.label}>Жанр:</p>,
        value: !!data?.genres?.length && (
          <div className={styles.valuesWrapper}>
            {data.genres.map(({ id, name }) => (
              <Link
                key={`${name}-${id}`}
                className={`${styles.value} ${styles.asLink}`}
                href={paths.catalog(
                  {
                    genres: String(id),
                  },
                  data.type_of_movie as MovieTypeNameEnum,
                )}
              >
                {name}
              </Link>
            ))}
          </div>
        ),
      },
      {
        label: <p className={styles.label}>Возраст:</p>,
        value: (
          <p className={styles.value}>{`+${
            (isNaN(Number(ageRating)) ? UnifiedMPAARating[ageRating] : ageRating) || 0
          }`}</p>
        ),
      },
    ].filter(({ value }) => !!value),
  );

  return (
    <div className={styles.root}>
      {fields.current.map(({ value, label }, index) => (
        <div className={styles.field} key={index}>
          {label}
          {value}
        </div>
      ))}
    </div>
  );
};
