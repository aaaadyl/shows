import { Title as NativeTitle } from 'shared/ui/title';
import styles from './styles.module.scss';
import { useStore } from 'effector-react';
import { commonMovieDetailModel } from 'entities/movie/model';
import { getPageTitle } from 'entities/movie/lib';
import { UnifiedMPAARating } from 'shared/config/ratings';

export const Title = () => {
  const data = useStore(commonMovieDetailModel.$movie);
  const ageRating = data?.age_rating as keyof typeof UnifiedMPAARating;
  return (
    <div className={styles.root}>
      <NativeTitle className={styles.title}>
        {getPageTitle(data?.title)} ({data?.year})
      </NativeTitle>
      <p className={styles.alternativeTitle}>
        {data?.title} <b>{(isNaN(Number(ageRating)) ? UnifiedMPAARating[ageRating] : data?.age_rating) || 0}+</b>
      </p>
    </div>
  );
};
