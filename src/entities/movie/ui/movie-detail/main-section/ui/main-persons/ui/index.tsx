import { useStore } from 'effector-react';
import styles from '../styles.module.scss';
import { AnimeStudios } from './anime-studios';
import { commonMovieDetailModel } from 'entities/movie/model';
import { MovieTypeIDEnum } from 'shared/movies';

export const MainPersons = () => {
  const data = useStore(commonMovieDetailModel.$movie);

  const renderItemByType = (type: MovieTypeIDEnum) => {
    switch (type) {
      case MovieTypeIDEnum.Anime:
        return <AnimeStudios />;
      default:
        return null;
    }
  };

  // const items = [
  //   { label: 'Режиссёр', list: getDirector(data?.directors ?? []) },
  //   { label: 'Актеры', list: getActors(data?.actors ?? []) },
  // ];

  return <div className={styles.root}>{data && renderItemByType(Number(data?.type_of_movie) as MovieTypeIDEnum)}</div>;
};
