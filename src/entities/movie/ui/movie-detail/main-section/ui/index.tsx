import clsx from 'clsx';
import { MainButtons } from './main-buttons';
import { MainPersons } from './main-persons';
import { MobileActions } from './mobile-actions';
import { ShareModal } from './share-modal';
import styles from './styles.module.scss';
import { Title } from './title';
import { TrailerModal } from './trailer-modal';
import Image from 'next/image';
import { useStore } from 'effector-react';
import { DetailFields } from './detail-fields';
import { commonMovieDetailModel } from 'entities/movie/model';

export const MainSection = () => {
  const data = useStore(commonMovieDetailModel.$movie);

  return (
    <section className={styles.section}>
      <div className={clsx('container', styles.container)}>
        <Image
          width="320"
          height="240"
          className={styles.poster}
          quality={100}
          alt={data?.title || ''}
          src={data?.poster_url || ''}
        />
        <div className={styles.content}>
          <Title />
          <DetailFields />
          <MobileActions />
          <MainPersons />
          <MainButtons />
        </div>
      </div>
      <TrailerModal />
      <ShareModal />
      {/* <Player movie_id={''} /> */}
    </section>
  );
};
