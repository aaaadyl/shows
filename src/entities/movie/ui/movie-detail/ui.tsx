import { useEvent, useStore } from 'effector-react';
import { NextSeo } from 'next-seo';
import { usePageEvent } from 'nextjs-effector';
import { MainSection } from './main-section';
import { SimilarMovies } from './similar-movies';
import { Description } from './description';
import { useRouter } from 'next/router';
import { useCallback, useRef } from 'react';
import { commonMovieDetailModel, kodikAnimeModel, kodikDoramaModel, moviePlayerModel } from '../../model';
import { Player } from './player';
import { KodikAnimeMovieParams } from './kodik-anime-movie-params';
import { KodikDoramaMovieParams } from './kodik-dorama-movie-params';
import { MovieTypeNameEnum } from 'shared/movies';
import { BazonMoviesParams } from './bazon-movies-params';
import { getBazonPlayerMovie } from 'entities/movie/model/bazon-movies';
import styles from './styles.module.scss';
import { PlayerAgregator } from './player-agregator';

export const MovieDetail = () => {
  usePageEvent(moviePlayerModel.events.initPlayer);
  usePageEvent(commonMovieDetailModel.events.clientStarted);
  const fullScreenWrapperRef = useRef<HTMLDivElement>(null);
  const { query } = useRouter();
  const data = useStore(commonMovieDetailModel.$movie);
  const year = data?.year ? `(${data?.year})` : '';
  const userIp = useStore(commonMovieDetailModel.$userExternalIP);
  const title = `${data?.title} ${year} смотреть онлайн бесплатно в хорошем HD 1080 / 720 качестве`;
  const setDoramyData = useEvent(kodikDoramaModel.setDoramyData);
  const getAnimeSeasonsFx = useEvent(kodikAnimeModel.getAnimeSeasonsFx);
  const movie_slug = query.slug as string;

  const renderMovieComponents = useCallback(() => {
    if (data?.id && userIp) {
      switch (data.type_of_movie) {
        case MovieTypeNameEnum.Anime:
          getAnimeSeasonsFx({ movie: String(data.id) });
          return (
            <>
              <Player fullScreenWrapperRef={fullScreenWrapperRef} />
              <KodikAnimeMovieParams movie_id={data.id} />
            </>
          );
        case MovieTypeNameEnum.Dorama:
          setDoramyData(data?.get_translation || null);
          return (
            <>
              <Player fullScreenWrapperRef={fullScreenWrapperRef} />
              <KodikDoramaMovieParams movie_id={data.id} />
            </>
          );
        case MovieTypeNameEnum.Film:
        case MovieTypeNameEnum.Cartoons:
        case MovieTypeNameEnum.Series:
          getBazonPlayerMovie({
            kp: String(data?.kinopoisk_id),
            ip: userIp?.ip,
          });
          return (
            <>
              <PlayerAgregator kinopoisk_id={String(data.kinopoisk_id)} />
              <BazonMoviesParams />
            </>
          );
        default:
          return null;
      }
    }

    return null;
  }, [data, movie_slug, userIp]);

  return (
    <>
      <NextSeo
        nofollow
        noindex
        title={title}
        description={data?.description || ''}
        canonical={`${process.env.BASE_URL}/${data?.type_of_movie}/${data?.id || ''}`}
        openGraph={{
          title,
          description: data?.description || '',
          images: [
            {
              url: data?.poster_url || '',
              alt: data?.title,
            },
          ],
        }}
      />
      <div ref={fullScreenWrapperRef} className={styles.fullscreenWrapper}>
        {renderMovieComponents()}
        <MainSection />
        <Description />
        <SimilarMovies />
      </div>
      {/* <Facts data={data?.description} /> */}
    </>
  );
};
