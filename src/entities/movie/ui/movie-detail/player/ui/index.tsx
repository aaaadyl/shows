import clsx from 'clsx';
import { useEvent, useStore } from 'effector-react';
import { RefObject, useEffect, useRef, useState } from 'react';
import ReactPlayer from 'react-player';
import { Spinner, Title } from 'shared/ui';
import { PlayerActions } from './player-actions';
import styles from './styles.module.scss';
import { BaseReactPlayerProps } from 'react-player/base';
import { moviePlayerModel } from 'entities/movie/model';
import { useToggler } from 'shared/lib';

interface Player {
  fullScreenWrapperRef: RefObject<HTMLDivElement> | null;
}

export const Player = ({ fullScreenWrapperRef }: Player) => {
  const fullscreen = useToggler(moviePlayerModel.fullscreenToggler);
  const playerInnerWrapperRef = useRef<HTMLDivElement>(null);
  const playerContainerRef = useRef<HTMLDivElement>(null);
  const playerRef = useRef<BaseReactPlayerProps & ReactPlayer>(null);
  const [cursorStopped, setCursorStopped] = useState(false);
  const [playerHeight, setPlayerHeight] = useState<number>(0);
  const { url, playing, readyToPlay, ls_key, muted, volume } = useStore(moviePlayerModel.$playerState);

  const handleDuration = useEvent(moviePlayerModel.events.handleDuration);
  const handleProgress = useEvent(moviePlayerModel.events.handleProgress);
  const handleReadyToPlay = useEvent(moviePlayerModel.events.handleReadyToPlay);

  useEffect(() => {
    let cursorTimer: any = 0;

    const handleMouseMove = () => {
      clearTimeout(cursorTimer);
      setCursorStopped(false);

      cursorTimer = setTimeout(() => {
        setCursorStopped(true);
      }, 1500);
    };
    window.addEventListener('mousemove', handleMouseMove);
    return () => {
      window.removeEventListener('mousemove', handleMouseMove);
    };
  }, []);

  useEffect(() => {
    playerContainerRef?.current && setPlayerHeight(playerContainerRef.current.offsetWidth);
  }, [playerContainerRef.current?.offsetWidth]);

  return (
    <div
      ref={playerInnerWrapperRef}
      style={{ cursor: playing && cursorStopped ? 'none' : 'unset' }}
      className={`${styles.root} ${!fullscreen.isOpen && 'container'}`}
    >
      <div
        ref={playerContainerRef}
        style={{
          height: fullscreen.isOpen ? '100vh' : `${((playerHeight || 0) * 56) / 100}px`,
        }}
        className={styles.playerWrapper}
      >
        {url && (
          <ReactPlayer
            key={String(url)}
            ref={playerRef}
            onReady={() => {
              handleReadyToPlay(true);
            }}
            controls={false}
            controlsList="nodownload"
            className={styles.player}
            onBuffer={() => handleReadyToPlay(false)}
            onBufferEnd={() => {
              handleReadyToPlay(true);
            }}
            muted={muted}
            volume={volume}
            playing={playing}
            onDuration={handleDuration}
            onProgress={(progress) => ls_key && handleProgress({ progress, ls_key })}
            width="100%"
            height="100%"
            pip={true}
            url={url}
          />
        )}
        <PlayerActions
          playerInnerWrapperRef={playerInnerWrapperRef}
          fullScreenWrapperRef={fullScreenWrapperRef}
          playerRef={playerRef}
          cursorStopped={cursorStopped && playing}
        />
        <div className={clsx(styles.spinner, !readyToPlay && styles.isLoading)}>
          <Spinner strokeWidth={5} />
        </div>
      </div>
    </div>
  );
};
