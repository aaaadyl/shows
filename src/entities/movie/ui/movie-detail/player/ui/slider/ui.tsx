import React, { RefObject, useMemo, useRef } from 'react';
import ReactPlayer from 'react-player';
import styles from './styles.module.scss';
import { useMouse } from 'react-use';
import { getDuration, updateProgressLS } from '../../../../../lib';
import { useEvent, useStore } from 'effector-react';
import { moviePlayerModel } from 'entities/movie/model';

interface ISlider {
  playerRef: RefObject<ReactPlayer>;
}

export const Slider = ({ playerRef }: ISlider) => {
  const sliderWrapper = useRef<HTMLDivElement>(null);
  const cursor = useMouse(sliderWrapper);
  const { duration, loadedSeconds, played, url } = useStore(moviePlayerModel.$playerState);
  const onSeekChange = useEvent(moviePlayerModel.events.handleSeekChange);
  const handleDirtyPlayer = useEvent(moviePlayerModel.events.handleDirtyPlayer);

  const { popValue, posLeft } = useMemo(() => {
    if (cursor) {
      const value = (cursor?.elX * duration) / cursor?.elW || 0;
      return { popValue: value, posLeft: (cursor?.elX * 100) / cursor?.elW };
    }
    return { popValue: 0, posLeft: 0 };
  }, [cursor, duration]);

  const handleMouseUp = (value: number) => {
    url && updateProgressLS(url, value * duration);
    playerRef.current?.seekTo(value);
    handleDirtyPlayer(true);
  };

  return (
    <div className={styles.sliderWrapper} ref={sliderWrapper}>
      <style>{`
            .${styles.slider}::after {
              width: ${((loadedSeconds || 0) * 100) / duration}%;
            }
            .${styles.slider}::before {
              width: ${played * 100}%;
            }
          `}</style>
      <div className={styles.seek} style={{ left: `${posLeft}%` }}>
        {getDuration(popValue)}
      </div>
      <input
        type="range"
        min={0}
        max={0.9999999}
        value={played}
        step="any"
        className={styles.slider}
        onMouseUp={(event) => {
          const { value } = event.target as HTMLInputElement;
          handleMouseUp(parseFloat(value));
        }}
        onChange={({ target: { value } }) => onSeekChange(parseFloat(String(value)))}
      />
    </div>
  );
};
