import { useEvent, useStore } from 'effector-react';
import styles from './styles.module.scss';
import clsx from 'clsx';
import { Button } from 'shared/ui';
import { moviePlayerModel } from 'entities/movie/model';

export const Quality = () => {
  const { quality, qualities } = useStore(moviePlayerModel.$playerState);

  const qualityClick = useEvent(moviePlayerModel.events.qualityClick);
  const handleQuality = (value: { quality: string; link: string }) => {
    qualityClick(value);
  };

  return (
    <div className={styles.qualityWrapper}>
      {qualities && (
        <ul className={styles.choices}>
          {qualities
            .sort((a, b) => Number(b.quality) - Number(a.quality))
            .map(({ quality: _q, link }) => (
              <li className={clsx([styles.choice])} key={`${_q}_${link}`}>
                <Button
                  variant={_q === quality ? 'white' : 'glass'}
                  size="small"
                  className={clsx([styles.qBtn, _q === quality && styles.selected])}
                  onClick={() =>
                    _q !== quality &&
                    handleQuality({
                      quality: _q,
                      link,
                    })
                  }
                >
                  {_q}
                </Button>
              </li>
            ))}
        </ul>
      )}
      <Button variant="primary" size="small" className={clsx([styles.selectedChoice, styles.qBtn])}>
        {quality}
      </Button>
    </div>
  );
};
