import { useOnClickOutside, useToggler } from 'shared/lib';
import styles from './styles.module.scss';
import { Button, Icon } from 'shared/ui';
import { useEffect, useRef, useState } from 'react';
import { useEvent, useStore } from 'effector-react';
import { moviePlayerModel } from 'entities/movie/model';
import clsx from 'clsx';

interface Volume {
  cursorStopped: boolean;
}

export function Volume({ cursorStopped }: Volume) {
  const wrapperRef = useRef(null);
  const toggleMute = useEvent(moviePlayerModel.events.mutePlayer);
  const { volume, muted } = useStore(moviePlayerModel.$playerState);
  const onChangeVolume = useEvent(moviePlayerModel.events.changeVolume);
  const [open, setOpen] = useState(false);

  useOnClickOutside(wrapperRef, () => setOpen(false));

  const onClickVolume = () => {
    if (open) {
      toggleMute(!muted);
    }
    setOpen(true);
  };

  useEffect(() => {
    if (cursorStopped) {
      setOpen(false);
    }
  }, [cursorStopped]);

  return (
    <div ref={wrapperRef} className={styles.wrapper}>
      <style>{`
            .${styles.ranger}::after {
              width: ${volume * 100}%;
            }
          `}</style>
      {open && (
        <div className={styles.rangeWrapper}>
          <input
            value={volume}
            max={0.9999999}
            min={0}
            step="any"
            className={clsx([styles.ranger, muted && styles.muted])}
            onChange={({ target: { value } }) => {
              toggleMute(false);
              onChangeVolume(Number(value));
            }}
            onMouseUp={(event) => {
              const { value } = event.target as HTMLInputElement;
              toggleMute(false);
              onChangeVolume(Number(value));
            }}
            type="range"
          />
        </div>
      )}
      <Button onClick={onClickVolume} variant="glass" className={styles.btn}>
        <Icon type="player" name={muted ? 'mute' : 'volume'} />
      </Button>
    </div>
  );
}
