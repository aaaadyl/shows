import { Button, Icon } from 'shared/ui';
import styles from './styles.module.scss';
import { useEvent, useStore } from 'effector-react';
import { RefObject } from 'react';
import ReactPlayer from 'react-player';
import { moviePlayerModel } from 'entities/movie/model';
import { getMovieLSProgress } from 'entities/movie/lib';

interface IPlay {
  playerRef: RefObject<ReactPlayer>;
}

export const Play = ({ playerRef }: IPlay) => {
  const { dirtyPlayer, ls_key } = useStore(moviePlayerModel.$playerState);
  const togglePlaying = useEvent(moviePlayerModel.events.togglePlaying);
  const handlePlaying = useEvent(moviePlayerModel.events.handlePlaying);
  const handleDirtyPlayer = useEvent(moviePlayerModel.events.handleDirtyPlayer);

  const lsProgress: number = getMovieLSProgress(String(ls_key));

  return (
    <div className={styles.playerContainer}>
      {!!lsProgress && !dirtyPlayer ? (
        <>
          <Button
            size="small"
            onClick={() => {
              playerRef?.current?.seekTo(Number(lsProgress));
              handleDirtyPlayer(true);
              handlePlaying(true);
            }}
          >
            Продолжить просмотр
          </Button>
          <Button
            size="small"
            variant="gray"
            onClick={() => {
              playerRef?.current?.seekTo(Number(0));
              handleDirtyPlayer(true);
              handlePlaying(true);
            }}
          >
            Смотреть сначала
          </Button>
        </>
      ) : (
        <Button
          size="small"
          variant="glass"
          className={styles.defaultPlatBtn}
          onClick={() => {
            handleDirtyPlayer(true);
            togglePlaying();
          }}
        >
          <Icon type="player" name="play" />
        </Button>
      )}
    </div>
  );
};
