import { Button, Icon, Title } from 'shared/ui';
import styles from './styles.module.scss';
import ReactPlayer, { ReactPlayerProps } from 'react-player';
import { MouseEvent, RefObject, useEffect, useState } from 'react';
import { Slider } from '../../slider';
import { getDuration, getMovieLSProgress } from '../../../../../../lib';
import { useEvent, useStore } from 'effector-react';
import { Play } from './play';
import { commonMovieDetailModel, moviePlayerModel } from 'entities/movie/model';
import { Quality } from './quality';
import { useToggler } from 'shared/lib';
import { searchModel } from 'entities/search-window';
import { Volume } from './volume';

interface IPlayerActions extends ReactPlayerProps {
  playerRef: RefObject<ReactPlayer>;
  cursorStopped: boolean;
  fullScreenWrapperRef: RefObject<HTMLDivElement> | null;
  playerInnerWrapperRef: RefObject<HTMLDivElement> | null;
}

export const PlayerActions = ({ playerRef, fullScreenWrapperRef, cursorStopped }: IPlayerActions) => {
  const fullscreen = useToggler(moviePlayerModel.fullscreenToggler);
  const movie = useStore(commonMovieDetailModel.$movie);
  const {
    duration,
    playing,
    played,
    playedSeconds,
    dirtyPlayer,
    ls_key,
    readyToPlay,
    hasNextEpisode,
    url,
    season,
    episode,
  } = useStore(moviePlayerModel.$playerState);

  const handlePlaying = useEvent(moviePlayerModel.events.handlePlaying);
  const [isIPhone, setIsIphone] = useState(false);
  const { isOpen } = useToggler(searchModel.toggler);

  useEffect(() => {
    // Access window.navigator.platform in a client-side context
    setIsIphone(window.navigator.platform?.includes('iphone'));
  }, []);

  const togglePlaying = useEvent(moviePlayerModel.events.togglePlaying);
  const handleDirtyPlayer = useEvent(moviePlayerModel.events.handleDirtyPlayer);
  const onClickNextEpisode = useEvent(moviePlayerModel.events.handleNextEpdisode);

  function exitFullscreen() {
    const doc: any = document;
    if (doc?.fullscreenElement) {
      if (doc?.exitFullscreen) {
        doc?.exitFullscreen();
      } else if (doc?.mozCancelFullScreen) {
        // Firefox
        doc?.mozCancelFullScreen();
      } else if (doc?.webkitExitFullscreen) {
        // Safari
        doc?.webkitExitFullscreen();
      } else if (doc?.msExitFullscreen) {
        // IE/Edge
        doc?.msExitFullscreen();
      }
    }
  }

  const enterFullscreen = () => {
    const wrapper: any = fullScreenWrapperRef?.current;
    if (wrapper && !isIPhone) {
      if (wrapper?.requestFullscreen) {
        wrapper?.requestFullscreen();
      } else if (wrapper?.mozRequestFullScreen) {
        // Firefox
        wrapper?.mozRequestFullScreen();
      } else if (wrapper?.webkitRequestFullscreen) {
        // Chrome, Safari, and Opera
        wrapper?.webkitRequestFullscreen();
      } else if (wrapper?.msRequestFullscreen) {
        // IE/Edge
        wrapper?.msRequestFullscreen();
      }
    } else if (playerRef?.current) {
      handlePlaying(true);
    }
  };

  const toggleFullScreen = (e?: MouseEvent<HTMLButtonElement>) => {
    if (isIPhone) {
      togglePlaying();
      return e?.preventDefault();
    }

    if (!fullscreen.isOpen) {
      enterFullscreen();
      fullscreen.open();
    } else {
      exitFullscreen();
      fullscreen.close();
    }
    return e?.preventDefault();
  };

  const rewindHandler = () => {
    handleDirtyPlayer(true);
    //Rewinds the video player reducing 10
    playerRef.current?.seekTo(playerRef.current.getCurrentTime() - 10);
  };

  const handleFastFoward = async () => {
    handleDirtyPlayer(true);
    //FastFowards the video player by adding 10
    playerRef.current?.seekTo(playerRef.current.getCurrentTime() + 10);
  };

  const lsProgress: number = getMovieLSProgress(String(ls_key));

  const handleKeyPress = (event: KeyboardEvent) => {
    switch (event.key) {
      case ' ':
      case 'Spacebar':
      case '32':
        if (!dirtyPlayer && lsProgress) {
          playerRef?.current?.seekTo(lsProgress);
        }
        togglePlaying();
        handleDirtyPlayer(true);
        event.preventDefault();
        break;
      case 'F':
      case 'f':
      case 'а':
      case 'А':
        toggleFullScreen();
        break;
      case 'ArrowLeft':
        rewindHandler();
        break;
      case 'ArrowRight':
        handleFastFoward();
        break;
      case 'Escape':
        close();
      default:
        break;
    }
  };

  useEffect(() => {
    playerRef.current?.seekTo(lsProgress || playedSeconds || 0);
  }, [url]);

  useEffect(() => {
    if (!isOpen) {
      window.addEventListener('keydown', handleKeyPress);
      return () => {
        window.removeEventListener('keydown', handleKeyPress);
      };
    }
  }, [playing, dirtyPlayer, url, fullscreen, readyToPlay, played, lsProgress, isOpen]);

  return (
    <>
      {!cursorStopped && (
        <Title as="h1" size="small" className={styles.movie_title}>
          {movie?.title}
        </Title>
      )}
      {!playing && <Play playerRef={playerRef} />}
      <div style={{ opacity: cursorStopped ? '0' : '1' }} className={styles.container}>
        <div className={styles.durationWrapper}>
          <p>{getDuration(duration * (played || 0))}</p>
          <p>{getDuration(duration)}</p>
        </div>
        <Slider playerRef={playerRef} />
        <div className={styles.actionPartsContainer}>
          <div className={styles.actionParts}>
            <Button
              variant="glass"
              className={styles.actionBtn}
              onClick={(event) => {
                event.preventDefault();
                togglePlaying();
              }}
            >
              <Icon type="player" name={!playing ? 'play' : 'pause'} />
            </Button>

            {hasNextEpisode && (
              <Button onClick={onClickNextEpisode} variant="glass" className={styles.actionBtn}>
                <Icon type="player" name="next" />
              </Button>
            )}

            <Button variant="glass" onClick={rewindHandler} className={styles.actionBtn}>
              <Icon type="player" name="replay-10" />
            </Button>

            <Button variant="glass" onClick={handleFastFoward} className={styles.actionBtn}>
              <Icon type="player" name="forward-10" />
            </Button>

            <Volume cursorStopped={cursorStopped} />
          </div>
          <div className={styles.actionParts}>
            <p className={styles.movie_params}>
              сезон: {season} серия: {episode}
            </p>
            <Quality />
            <Button variant="glass" className={styles.actionBtn} onClick={toggleFullScreen}>
              <Icon type="player" name={fullscreen.isOpen && !isIPhone ? 'minimize' : 'maximize'} />
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};
