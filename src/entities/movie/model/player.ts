import { createEffect, createEvent, createStore, sample } from 'effector';
import { PageContext } from 'nextjs-effector';
import { updateProgressLS } from '../lib';
import { createToggler } from 'shared/lib';
import { commonApi } from 'shared/api';

interface PlayerProgress {
  loaded: number;
  loadedSeconds: number;
  played: number;
  playedSeconds: number;
}

interface Quality {
  link: string;
  quality: string;
}

interface SetMovieParams {
  episode: string;
  season: string;
  hasNextEpisode: boolean;
}

interface PlayerState {
  readyToPlay: boolean;
  qualities: Quality[] | null;
  quality: string | null;
  url: string | null;
  pip: boolean;
  playing: boolean;
  light: boolean;
  volume: number;
  played: number;
  loaded: number;
  playedSeconds: number;
  loadedSeconds: number;
  fullscreen: boolean;
  duration: number;
  dirtyPlayer: boolean;
  ls_key: string | null;
  season: string | null;
  episode: string | null;
  hasNextEpisode: boolean;
  muted: boolean;
}

const initialState: PlayerState = {
  readyToPlay: false,
  qualities: null,
  quality: null,
  url: null,
  pip: false,
  playing: false,
  light: false,
  volume: 0.8,
  played: 0,
  loaded: 0,
  playedSeconds: 0,
  loadedSeconds: 0,
  duration: 0,
  fullscreen: false,
  dirtyPlayer: false,
  ls_key: null,
  season: null,
  episode: null,
  hasNextEpisode: false,
  muted: false,
};

export const setKodikQualities = createEffect((link: string) => {
  return commonApi.movies.moviesKodicCreate({ url: link });
});

export const handleNextEpdisode = createEvent();

export const setMovieParams = createEvent<SetMovieParams>();

const handleKodikEpisodeChange = createEvent<string>();
const setLSKey = createEvent<string>();
const initPlayer = createEvent<PageContext>();
interface IProgressUpdate {
  ls_key: string;
  progress: PlayerProgress;
}

const mutePlayer = createEvent<boolean>();
const clearState = createEvent();
const changeVolume = createEvent<number>();
const qualityClick = createEvent<{ quality: string; link: string }>();
const handleReadyToPlay = createEvent<boolean>();
const handleSeekChange = createEvent<number>();
const handleProgress = createEvent<IProgressUpdate>();
const handleDuration = createEvent<number>();
const togglePlaying = createEvent();
const handlePlaying = createEvent<boolean>();
const handleDirtyPlayer = createEvent<boolean>();

export const fullscreenToggler = createToggler();

export const $playerState = createStore<PlayerState>(initialState)
  .on(clearState, (state) => ({ ...initialState, quality: state.quality }))
  .on(handleReadyToPlay, (state, payload) => ({ ...state, readyToPlay: payload }))
  .on(handleSeekChange, (state, payload) => ({ ...state, played: payload }))
  .on(handleProgress, (state, { progress }) => {
    const { ls_key } = state;
    if (ls_key && state.readyToPlay && state.playing) {
      updateProgressLS(ls_key, `${progress.playedSeconds}`);
    }
    return { ...state, ...progress };
  })
  .on(handleDuration, (state, payload) => {
    return { ...state, duration: payload };
  })
  .on(changeVolume, (state, payload) => {
    return { ...state, volume: payload };
  })
  .on(qualityClick, (state, payload) => ({
    ...state,
    playing: false,
    readyToPlay: false,
    quality: payload.quality,
    url: payload?.link,
  }))
  .on(togglePlaying, (state) => ({ ...state, playing: !state.playing }))
  .on(handleDirtyPlayer, (state, payload) => ({ ...state, dirtyPlayer: payload }))
  .on(handlePlaying, (state, payload) => ({ ...state, playing: payload }))
  .on(setMovieParams, (state, { season, episode, hasNextEpisode }) => {
    if (season && episode) {
      return { ...state, season, episode, hasNextEpisode };
    }
    return state;
  })
  .on(mutePlayer, (state, payload) => {
    return { ...state, muted: payload };
  })
  .on(setKodikQualities.doneData, (state, payload) => {
    if (payload.links) {
      const qualities = Object.entries(payload.links)
        .map(([quality, { Src }]) => ({ quality, link: Src }))
        .filter(({ link, quality }) => !!(link && quality));

      const lastQuality = qualities[qualities.length - 1];
      const prevQuality = state.quality && qualities.find(({ quality }) => quality === state.quality);
      if (prevQuality) {
        return { ...state, quality: prevQuality.quality, qualities, url: prevQuality.link };
      }
      return { ...state, quality: lastQuality.quality, qualities, url: lastQuality.link };
    }
    return state;
  })
  .on(setLSKey, (state, payload) => {
    return { ...state, ls_key: payload };
  });

handleKodikEpisodeChange.watch((payload) => {
  clearState();
  setKodikQualities(payload);
});

sample({
  clock: initPlayer,
  target: [clearState],
});

export const events = {
  initPlayer,
  clearState,
  qualityClick,
  handleReadyToPlay,
  handleSeekChange,
  handleProgress,
  handleDuration,
  togglePlaying,
  handlePlaying,
  handleDirtyPlayer,
  handleKodikEpisodeChange,
  setLSKey,
  setMovieParams,
  changeVolume,
  handleNextEpdisode,
  mutePlayer,
};
