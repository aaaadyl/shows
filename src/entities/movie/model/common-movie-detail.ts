import { attach, createEffect, createEvent, createStore, restore, sample } from 'effector';
import type { PageContext } from 'nextjs-effector';
import { favoritesModel } from 'features/favorites';
import { commonApi, internalApi } from 'shared/api';
import { createToggler } from 'shared/lib/toggler';
import { RatingsEnum } from 'shared/config/ratings';
import axios, { AxiosResponse } from 'axios';

// GETTING USER OUTER IP
const getExternalDataFx = createEffect(async () => {
  const url = process.env.API_IPIFY_URL || '';
  const { data }: AxiosResponse<{ ip: string }> = await axios.get(url);
  return data;
});

export const $userExternalIP = restore(getExternalDataFx, null);

export const fetchMainMovie = createEvent<PageContext>();
export const clientStarted = createEvent();

export const trailerToggler = createToggler();
export const shareToggler = createToggler();
export const playerToggler = createToggler();

const getMovieBySlugFx = attach({ effect: commonApi.movies.moviesMoviesRead });
//!!! const checkFavoriteFx = attach({ effect: internalApi.checkFavorite });

// Attach the effect to films event, anime, dorama, series, cartoons event to get their details
fetchMainMovie.watch(async ({ params }) => {
  getMovieBySlugFx(String(params.slug));
});

export const $movie = restore(getMovieBySlugFx, null);

interface MovieRatings {
  votes: number;
  rating: number;
  name: RatingsEnum;
}

export const $ratings = createStore<MovieRatings[] | null>(null).on(getMovieBySlugFx.doneData, (_, payload) => {
  if (payload) {
    return [
      {
        name: RatingsEnum.imdb,
        rating: Number(payload?.imdb_rating),
        votes: Number(payload?.imdb_votes),
      },
      {
        name: RatingsEnum.kp,
        rating: Number(payload?.kinopoisk_rating),
        votes: Number(payload?.kinopoisk_votes),
      },
    ].filter(({ rating }) => !!rating);
  }
  return null;
});

// sample({
//   clock: checkFavoriteFx.doneData,
//   target: favoritesModel.$isFavorite,
// });

sample({
  clock: [clientStarted],
  target: getExternalDataFx,
});

export const events = { fetchMainMovie, clientStarted };
