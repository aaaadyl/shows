import { attach, createEvent, createStore, restore, sample } from 'effector';
import type { PageContext } from 'nextjs-effector';
import { BazonTranslation, BazonMovieSeason, bazonApi, BazonMovieQuality, BazonMovieEpisodes } from 'shared/api';

interface BazonState {
  qualities: BazonMovieQuality | null;
  selectedTranslation: BazonTranslation | null;
  translations: BazonTranslation[] | null;
  isSeries: boolean;
  selectedSerie: string | string;
  selectedSeason: string | string;
  seasons: BazonMovieSeason | null;
  series: BazonMovieEpisodes | null;
}

export const clientStarted = createEvent<PageContext>();

// PLAYER EVENTS
export const moviePageStarted = createEvent<PageContext>();
export const clearState = createEvent();
export const translationClicked = createEvent<string>();
export const seasonsClick = createEvent<string>();
export const episodesClick = createEvent<string>();

export const getBazonPlayerMovie = attach({ effect: bazonApi.getMoviePlayer });

export const $playerMovie = restore(getBazonPlayerMovie, null);

const initialState: BazonState = {
  qualities: null,
  translations: null,
  selectedTranslation: null,
  isSeries: false,
  selectedSerie: '0',
  selectedSeason: '0',
  seasons: null,
  series: null,
};

export const $bazonParamsState = createStore<BazonState>(initialState)
  .on(clearState, () => initialState)
  .on(getBazonPlayerMovie.done, (_, { params, result: { results } }) => {
    const selectedTranslation = results?.length ? results[0] : null;

    if (selectedTranslation) {
      const playlists = selectedTranslation?.playlists;
      const isSeries = !!Number(selectedTranslation?.serial);

      if (isSeries && playlists) {
        const [firstSeasonKey, firstSeasonEpisodes] = Object.entries(playlists as BazonMovieSeason);
        if (typeof firstSeasonEpisodes === 'object' && firstSeasonKey) {
          const seasons = firstSeasonEpisodes;
          // const episodeKey = firstSeasonKey && Object.keys(playlists[firstSeasonKey])?.[0];
          // const qualities = seasons[episodeKey];

          // return {
          //   ...initialState,
          //   qualities,
          //   selectedTranslation,
          //   isSeries: true,
          //   selectedSeason: firstSeasonKey,
          //   selectedSerie: episodeKey,
          //   seasons: seasons,
          // };
        }
      } else {
        const qualities = playlists || null;
        // return { ...initialState, qualities, translation, isSeries: false, selectedSeason: 0, selectedSerie: 0 };
      }
    }
  })
  .on(translationClicked, (state, payload) => {
    const translation = $playerMovie.getState()?.results.find((trans) => trans?.date === payload) || null;
    return { ...state, translation, readyToPlay: false };
  })
  .on(seasonsClick, (state, payload) => ({
    ...state,
    // playing: false,
    // url: getMovieUrl(
    //   state.translation?.playlists,
    //   state.isSeries,
    //   payload,
    //   String(state.episode),
    //   String(state.quality),
    // ),
    // readyToPlay: false,
    // dirtyPlayer: false,
    // selectedSeason: payload,
  }))
  .on(episodesClick, (state, payload) => ({
    ...state,
    // playing: false,
    // readyToPlay: false,
    // dirtyPlayer: false,
    // episode: payload,
  }));
