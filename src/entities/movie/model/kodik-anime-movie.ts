import { attach, createEvent, createStore } from 'effector';
import { Season, commonApi } from 'shared/api';
import { KodikTranslations } from 'shared/api/kodik';
import { commonMovieDetailModel, moviePlayerModel } from '.';
import { MovieTypeIDEnum } from 'shared/movies';

// receives dorama data
export const getAnimeSeasonsFx = attach({ effect: commonApi.movies.moviesSeasonsList });

interface KodikAnimeMovieState {
  seasons: Season[] | null;
  translations: KodikTranslations[] | null;
  selectedSeason: string | null; // selected season key
  selectedTranslation: string | null; // selected translation id
  series: Record<string, string> | null;
  selectedSerie: string | null; // selected episode key
}

export const kodikAnimeMovieInitialState: KodikAnimeMovieState = {
  seasons: null,
  translations: null,
  selectedSeason: null,
  selectedTranslation: null,
  series: null,
  selectedSerie: null,
};

const selectSeason = createEvent<string>(); // should be passed slug of selected anime season
const selectTranslation = createEvent<string>(); // should be passed id of selected anime translation
const selectSerie = createEvent<string>(); // should be passed id of selected anime translation

export const $kodikAnimeMovieState = createStore<KodikAnimeMovieState>(kodikAnimeMovieInitialState)
  .on(getAnimeSeasonsFx.doneData, (state, payload) => {
    const seasons = payload.results.map(({ get_translation, ...others }) => {
      const trsl = get_translation?.map(({ seasons, ..._s }) => {
        delete seasons?.['0'];
        return { ..._s, seasons: seasons };
      });
      return { ...others, get_translation: trsl };
    });

    const firstSeasonTranslations = seasons[0]?.get_translation || null;

    if (seasons?.length && firstSeasonTranslations?.length) {
      const firstTranslation = firstSeasonTranslations?.[0];
      const [_, firstSeason] = Object.entries(firstTranslation?.seasons)[0];
      const firstSerieKey = Object.keys(firstSeason.episodes)[0];
      return {
        ...state,
        seasons: seasons,
        selectedSeason: seasons[0].title,
        translations: firstSeasonTranslations,
        selectedTranslation: firstSeasonTranslations[0]?._id,
        series: firstSeason.episodes,
        selectedSerie: firstSerieKey,
      };
    }
    return kodikAnimeMovieInitialState;
  })
  .on(selectSeason, (state, payload) => {
    const { seasons } = state;
    const season = seasons?.find(({ title }) => title === payload);
    const trsl = season?.get_translation;
    if (season && trsl?.length) {
      const { episodes } = Object.entries(trsl[0].seasons)[0][1];
      const firstSerieKey = Object.keys(episodes)[0];
      return {
        ...state,
        selectedSeason: payload,
        translations: trsl,
        selectedTranslation: trsl[0]?._id,
        series: episodes,
        selectedSerie: firstSerieKey,
      };
    }
    return state;
  })
  .on(selectTranslation, (state, payload) => {
    const { seasons, selectedSeason } = state;
    const season = seasons?.find(({ title }) => title === selectedSeason);
    const trsl = season?.get_translation?.find(({ _id }) => _id === payload);
    if (season && trsl) {
      const { episodes } = Object.entries(trsl.seasons)[0][1];
      return { ...state, selectedTranslation: payload, series: episodes, selectedSerie: Object.keys(episodes)[0] };
    }
    return state;
  })
  .on(selectSerie, (state, serie) => {
    return { ...state, selectedSerie: serie };
  });

moviePlayerModel?.events?.handleNextEpdisode?.watch(() => {
  const { series, selectedSerie } = $kodikAnimeMovieState.getState();
  if (series && selectedSerie) {
    const seriesKeys = Object.keys(series);
    const nextSerie = seriesKeys[seriesKeys.indexOf(selectedSerie) + 1];
    selectSerie(nextSerie);
  }
});

// Player model related actions
$kodikAnimeMovieState.watch((state) => {
  const movie = commonMovieDetailModel.$movie?.getState();
  const { translations, selectedSeason, series, selectedSerie, selectedTranslation } = state;
  const _trsl = translations?.find(({ _id }) => _id === selectedTranslation);
  if (_trsl?.seasons && selectedSerie && selectedSeason && series && movie) {
    const seriesKeys = Object.keys(series);
    const lastSerie = seriesKeys[seriesKeys.length - 1];

    const { episodes } = Object.entries(_trsl?.seasons)[0][1];
    moviePlayerModel.events.handleKodikEpisodeChange(episodes[selectedSerie]);
    moviePlayerModel.events.setMovieParams({
      season: selectedSeason,
      episode: selectedSerie,
      hasNextEpisode: lastSerie !== selectedSerie,
    });
    const ls_key = `${MovieTypeIDEnum.Anime}_${movie?.id}_${selectedSeason}_${selectedSerie}`;
    moviePlayerModel.events.setLSKey(ls_key);
  }
});

export const events = { selectSeason, selectTranslation, selectSerie };
