import { createEvent, createStore } from 'effector';
import { KodikTranslations } from 'shared/api/kodik';
import { commonMovieDetailModel, moviePlayerModel } from '.';
import { MovieTypeIDEnum } from 'shared/movies';

// receives dorama data
export const setDoramyData = createEvent<KodikTranslations[] | null>();

interface KodikMovieState {
  seasons: string[] | null;
  translations: KodikTranslations[] | null;
  selectedSeason: string | null; // selected season key
  selectedTranslation: string | null; // selected translation id
  selectedSerie: string | null; // selected episode key
  series: Record<string, string> | null;
}

export const kodikMovieInitialState: KodikMovieState = {
  seasons: null,
  translations: null,
  selectedSeason: null,
  selectedTranslation: null,
  selectedSerie: null,
  series: null,
};

const selectSeason = createEvent<string>(); // should be passed slug of selected anime season
const selectTranslation = createEvent<string>(); // should be passed id of selected anime translation
const selectSerie = createEvent<string>(); // should be passed id of selected anime translation

export const $kodikDoramaState = createStore<KodikMovieState>(kodikMovieInitialState)
  .on(setDoramyData, (state, payload) => {
    const translations = payload?.length && payload.map(({ seasons, ...data }) => ({ ...data }));
    const seasons = payload?.[0].seasons;
    const seasonsKeys = Object.keys(seasons || {});
    const firstSeasonEpisodes = seasons?.[seasonsKeys[0]].episodes;
    if (translations && seasonsKeys.length && firstSeasonEpisodes) {
      const firstSerieKey = Object.keys(firstSeasonEpisodes)[0];
      return {
        ...state,
        translations: payload,
        seasons: seasonsKeys,
        selectedTranslation: String(translations[0]._id),
        selectedSeason: seasonsKeys[0],
        selectedSerie: firstSerieKey,
        series: firstSeasonEpisodes,
      };
    }
    return state;
  })
  .on(selectTranslation, (state, payload) => {
    const { translations } = state;
    const selectedTrsl = translations?.find(({ _id }) => _id === payload);
    if (translations && selectedTrsl) {
      const seasonsKeys = Object.keys(selectedTrsl.seasons);
      const firstSeasonEpisodes = selectedTrsl.seasons?.[seasonsKeys[0]].episodes;
      const firstSerieKey = Object.keys(firstSeasonEpisodes)[0];
      return {
        ...state,
        seasons: Object.keys(selectedTrsl.seasons),
        selectedTranslation: String(translations[0]._id),
        selectedSeason: seasonsKeys[0],
        selectedSerie: firstSerieKey,
        series: firstSeasonEpisodes,
      };
    }
    return { ...state, selectedTranslation: payload };
  })
  .on(selectSeason, (state, payload) => {
    const { translations, selectedTranslation } = state;
    const selectedTrsl = translations?.find(({ _id }) => _id === selectedTranslation);
    const selectedSeasonEpisodes = selectedTrsl?.seasons?.[payload]?.episodes;
    if (selectedSeasonEpisodes) {
      const firstSerieKey = Object.keys(selectedSeasonEpisodes)[0];
      return { ...state, selectedSeason: payload, series: selectedSeasonEpisodes, selectedSerie: firstSerieKey };
    }
  })
  .on(selectSerie, (state, serie) => {
    return { ...state, selectedSerie: serie };
  });

moviePlayerModel?.events?.handleNextEpdisode?.watch(() => {
  const { series, selectedSerie } = $kodikDoramaState.getState();
  if (series && selectedSerie) {
    const seriesKeys = Object.keys(series);
    const nextSerie = seriesKeys[seriesKeys.indexOf(selectedSerie) + 1];
    selectSerie(nextSerie);
  }
});

$kodikDoramaState.watch((state) => {
  const user = commonMovieDetailModel?.$userExternalIP?.getState();
  const movie = commonMovieDetailModel?.$movie?.getState();
  const { selectedSeason, selectedSerie, series } = state;
  if (selectedSerie && selectedSeason && user && movie && series) {
    const seriesKeys = Object.keys(series);
    const lastSerie = seriesKeys[seriesKeys.length - 1];

    moviePlayerModel.events.handleKodikEpisodeChange(series[selectedSerie]);
    moviePlayerModel.events.setMovieParams({
      season: selectedSeason,
      episode: selectedSerie,
      hasNextEpisode: lastSerie !== selectedSerie,
    });
    const ls_key = `${MovieTypeIDEnum.Anime}_${movie?.id}_${selectedSeason}_${selectedSerie}`;
    moviePlayerModel.events.setLSKey(ls_key);
  }
});

export const events = { selectSeason, selectTranslation, selectSerie };
