export function getMovieLSProgress(url: string) {
  const lsFilmsJson = typeof window !== 'undefined' && localStorage?.getItem('progresses');
  const lsProgressesF = lsFilmsJson ? JSON?.parse(lsFilmsJson) : null;
  return Number(lsProgressesF?.[url]);
}
