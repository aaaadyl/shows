const lsProgresses = 'progresses';

export const updateProgressLS = (url: string, newValue: string | number) => {
  const existingData: Record<string, string | number> = JSON.parse(localStorage.getItem(lsProgresses) || '{}');
  existingData[url] = newValue;

  localStorage.setItem(lsProgresses, JSON.stringify(existingData));
};
