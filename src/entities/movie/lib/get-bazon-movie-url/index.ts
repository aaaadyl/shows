import { BazonMovieQuality, BazonMovieSeason } from 'shared/api';

export function getBazonMovieUrl(
  playlist: BazonMovieQuality | BazonMovieSeason | undefined,
  isSeries: boolean,
  season: string | number,
  episode: string | number,
  quality: string | number,
) {
  if (playlist) {
    if (isSeries) {
      const _season = playlist?.[season];
      if (typeof _season === 'object') {
        return _season?.[episode]?.[quality];
      }
    } else {
      const _qualityUrl = playlist?.[quality];
      if (typeof _qualityUrl === 'string') {
        return _qualityUrl;
      }
    }
  }
  return '';
}
