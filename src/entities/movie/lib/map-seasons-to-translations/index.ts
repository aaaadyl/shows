import { Season } from 'shared/api';

export function mapSeasonsToTranslation(seasons: Season[]): Season[] | null {
  if (seasons.length) {
    const mapped = seasons.map((season) => {
      const seasonTitle = season.title;
      const get_translation = season?.get_translation?.map((translation) => {
        const seasonsEntries = Object.entries(translation.seasons);
        return {
          ...translation,
          seasons: {
            [seasonTitle]: seasonsEntries[0][1],
          },
        };
      });
      return {
        ...season,
        get_translation,
      };
    });
    return mapped;
  }

  return null;
}
