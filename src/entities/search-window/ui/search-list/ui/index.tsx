import clsx from 'clsx';
import { useStore } from 'effector-react';
import { searchModel } from 'entities/search-window';
import { Title, Spinner } from 'shared/ui';
import { SearchItem } from './search-item';
import styles from './styles.module.scss';
import { useMemo } from 'react';

export const SearchList = () => {
  const movies = useStore(searchModel.$moviesSearchResult);
  const pending = useStore(searchModel.$pending);

  const NoResultsMessage = (
    <>
      <Title className={styles.title} size="small">
        Ничего не нашлось
      </Title>
      <p className={styles.desc}>Может быть, вы ищете то, чего пока нет в каталоге</p>
    </>
  );

  const options = useMemo(() => {
    return movies?.title__completion?.[0]?.options || [];
  }, [movies]);

  if (!pending && !options.length) return NoResultsMessage;
  const SearchList = (
    <ul className={clsx('list-reset', styles.list)}>
      {options.map((item) => (
        <SearchItem key={item._id} item={item} />
      ))}
    </ul>
  );

  const Loader = (
    <div className={styles.loader}>
      <Spinner strokeWidth={2} />
    </div>
  );

  return pending ? Loader : SearchList;
};
