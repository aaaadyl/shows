import Image from 'next/image';
import Link from 'next/link';
import { getRating } from 'shared/lib';
import { paths } from 'shared/routing';
import { MovieRating } from 'shared/ui/movie-rating';
import styles from './styles.module.scss';
import { MovieSearchOptions } from 'shared/api';
import { MovieTypeNameEnum } from 'shared/movies';

interface SearchItemProps {
  item: MovieSearchOptions;
}

export const SearchItem = ({ item }: SearchItemProps) => {
  const { title, slug, poster_url, type_of_movie, year, imdb_rating } = item._source;

  return (
    <li className={styles.item}>
      <Link className={styles.link} href={paths.movie(slug, type_of_movie.name)}>
        <div className={styles.image}>
          {poster_url && <Image sizes="100%" fill quality={100} alt={title ?? ''} src={poster_url} />}
        </div>
        <div className={styles.text}>
          <span className={styles.name}>{title}</span>
          <div className={styles.info}>
            <span className={styles.year}>{year}</span>
            <MovieRating size="small" showState>
              {getRating(imdb_rating)}
            </MovieRating>
          </div>
        </div>
      </Link>
    </li>
  );
};
