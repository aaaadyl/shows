import clsx from 'clsx';
import { useStore } from 'effector-react';
import { searchModel } from 'entities/search-window';
import { useToggler } from 'shared/lib/toggler';
import { Modal } from 'shared/ui';
import { Icon } from 'shared/ui/icon';
import { SearchInput } from './search-input';
import { SearchList } from './search-list';
import styles from './styles.module.scss';

export const SearchWindow = () => {
  const toggler = useToggler(searchModel.toggler);
  const debouncedValue = useStore(searchModel.$debouncedValue);

  return (
    <Modal onKeyDown={(e) => e} isOpen={toggler.isOpen} close={toggler.close} className={styles.window}>
      <button className={clsx('btn-reset', styles.close)} onClick={toggler.close}>
        <Icon type="common" name="close" />
      </button>
      <div className={styles.container}>
        <SearchInput />
        {debouncedValue && <SearchList />}
      </div>
    </Modal>
  );
};
