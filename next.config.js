const nextConfig = {
  httpAgentOptions: {
    keepAlive: true,
  },
  async rewrites() {
    return [
      {
        source: '/sitemap.xml',
        destination: '/api/sitemap',
      },
      {
        source: '/robots.txt',
        destination: '/api/robots',
      },
    ];
  },
  images: {
    unoptimized: true,
    domains: [
      'st.kp.yandex.net',
      'avatars.mds.yandex.net',
      'themoviedb.org',
      'bazonserver.site',
      'get.bazon.to',
      'shows-bucket.s3.ap-southeast-1.amazonaws.com',
      'shows-bucket.fra1.digitaloceanspaces.com',
    ],
  },
  env: {
    BASE_URL: process.env.BASE_URL,
    API_TOKEN: process.env.API_KP_TOKEN,
    API_URL: process.env.API_URL,
    CLIENT_URL: process.env.CLIENT_URL,
    INTERNAL_API_URL: process.env.INTERNAL_API_URL,
    YM_TOKEN: process.env.YM_TOKEN,
    GA_TOKEN: process.env.GA_TOKEN,
    API_BN_TOKEN: process.env.API_BN_TOKEN,
    API_BN_URL: process.env.API_BN_URL,
    API_IPIFY_URL: process.env.API_IPIFY_URL,
    API_KODIK_URL: process.env.API_KODIK_URL,
    API_KODIK_PUBLIC_TOKEN: process.env.API_KODIK_PUBLIC_TOKEN,
    API_KODIK_PRIVATE_TOKEN: process.env.API_KODIK_PRIVATE_TOKEN,
  },
};

if (process.env.NODE_ENV === 'development') {
  console.log('info  - lanUrl:', `http://${require('address').ip()}:3000`);
}

module.exports = nextConfig;
